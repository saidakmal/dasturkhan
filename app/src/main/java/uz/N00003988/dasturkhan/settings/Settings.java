package uz.N00003988.dasturkhan.settings;

import android.content.Context;
import android.content.SharedPreferences;

public class Settings {
    private static Settings settings;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private Settings(Context context) {
        sharedPreferences = context.getSharedPreferences("NAME", Context.MODE_PRIVATE);
    }

    public static void init(Context context) {
        if (settings == null) {
            settings = new Settings(context);
        }
    }

    public static Settings getSettings() {
        return settings;
    }

    public void setToken(String token) {
        editor = sharedPreferences.edit();
        editor.putString("TOKENN", token);
        editor.apply();
    }

    public String getToken() {
        return sharedPreferences.getString("TOKENN", "tokenn");
    }

}
