package uz.N00003988.dasturkhan.room.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


@Entity(tableName = "favorites")
public class FavoriteModel {
    @PrimaryKey
    @ColumnInfo(name = "_id")
    @NonNull
    private String id;
    private int favorite;

    @Ignore
    public FavoriteModel() {
    }

    public FavoriteModel(@NonNull String id, int favorite) {
        this.id = id;
        this.favorite = favorite;
    }


    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public int getFavorite() {
        return favorite;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }
}
