package uz.N00003988.dasturkhan.room.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;


import uz.N00003988.dasturkhan.room.dao.FavoriteDao;
import uz.N00003988.dasturkhan.room.model.FavoriteModel;


@Database(entities = {FavoriteModel.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract FavoriteDao getFavoriteDao();
}
