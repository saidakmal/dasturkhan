package uz.N00003988.dasturkhan.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import uz.N00003988.dasturkhan.room.model.FavoriteModel;


@Dao
public interface FavoriteDao {
    @Query("select * from favorites where favorite = 1")
    List<FavoriteModel> getFavorites();

    @Query("select * from favorites where _id =:id ")
    FavoriteModel isFavourite(String id);

    //    @Query("select * from last_data where favorite = 1  order by position desc")
//    List<RecentlyViewedData> getFavoriteData();
//
//    @Query("select * from last_data where name LIKE :name")
//    RecentlyViewedData getData(String name);
//
    @Delete
    void deleteData(FavoriteModel data);

    @Insert()
    void insertFavoriteData(FavoriteModel... data);


}
