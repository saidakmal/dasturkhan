package uz.N00003988.dasturkhan.room.database;

import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

public class Database {
    private static AppDatabase database;

    public static void init(Context context) {
        if (database == null) {
            database = Room.databaseBuilder(context, AppDatabase.class, "data")
                    .allowMainThreadQueries()
                    .build();
        }
    }

    public static AppDatabase getDatabase() {
        return database;
    }
}
