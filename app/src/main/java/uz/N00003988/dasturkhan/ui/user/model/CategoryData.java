package uz.N00003988.dasturkhan.ui.user.model;

import android.support.annotation.DrawableRes;

import java.io.Serializable;

public class CategoryData implements Serializable {
    private String category;
    private int products;
    @DrawableRes
    private int src;

    public CategoryData() {
    }

    public CategoryData(String category, int products, @DrawableRes int src) {
        this.category = category;
        this.products = products;
        this.src = src;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getProducts() {
        return products;
    }

    public void setProducts(int products) {
        this.products = products;
    }

    public int getSrc() {
        return src;
    }

    public void setSrc(@DrawableRes int src) {
        this.src = src;
    }
}
