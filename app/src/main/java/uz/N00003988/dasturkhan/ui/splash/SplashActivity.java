package uz.N00003988.dasturkhan.ui.splash;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.ui.login.LoginActivity;

public class SplashActivity extends AppCompatActivity {
    private LinearLayout group;
    private TextView title;
    private TextView sub;
    private String s1 = "Know better, Book better";
    private String s2 = "Say hello to the dasturkhan !";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        group = findViewById(R.id.group);
        title = findViewById(R.id.splash_text);
        sub = findViewById(R.id.sub_text);
        loadAnimate();
    }

    private void loadAnimate() {
        sub.setAlpha(1.0f);
        sub.setText(s1);
        sub
                .animate()
                .translationY(-40f)
                .alpha(0.0f)
                .setStartDelay(500)
                .setDuration(1000)
                .withEndAction(() -> updateText())
                .start();

    }

    private void updateText() {
        sub.setY(sub.getY() + 60);
        sub.setText(s2);
        sub
                .animate()
                .translationY(sub.getY() - 20)
                .alpha(1.0f)
                .setStartDelay(0)
                .setDuration(1000)
                .withEndAction(() -> endAnimate())
                .start();
    }

    private void endAnimate() {
        group.animate()
                .scaleXBy(3.0f)
                .scaleYBy(3.0f)
                .alpha(0.0f)
                .setStartDelay(500)
                .setDuration(500)
                .withEndAction(() -> finishAnimate())
                .start();
    }

    private void finishAnimate() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
