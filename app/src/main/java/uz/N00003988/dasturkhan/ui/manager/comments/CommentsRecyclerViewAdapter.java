package uz.N00003988.dasturkhan.ui.manager.comments;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Rating;

public class CommentsRecyclerViewAdapter extends RecyclerView.Adapter<CommentsRecyclerViewHolder> {
    private ArrayList<Rating> ratings;

    public CommentsRecyclerViewAdapter(ArrayList<Rating> ratings) {
        this.ratings = ratings;
    }

    @NonNull
    @Override
    public CommentsRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new CommentsRecyclerViewHolder(
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_comments_layout, viewGroup, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsRecyclerViewHolder commentsRecyclerViewHolder, int i) {
        commentsRecyclerViewHolder.binData(ratings.get(i));

    }

    @Override
    public int getItemCount() {
        return ratings.size();
    }
}
