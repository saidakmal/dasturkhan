package uz.N00003988.dasturkhan.ui.main.holders;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Data;

public class ManagerDataViewHolder extends RecyclerView.ViewHolder {
    private TextView name;
    private ImageView imageView;
    private OnItemClickListener onItemClickListener;
    private ImageView addImageView;

    public ManagerDataViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener) {
        super(itemView);
        name = itemView.findViewById(R.id.name_item_manager);
        imageView = itemView.findViewById(R.id.image_view_item_manager_data);
        addImageView = itemView.findViewById(R.id.image_add_table_manager_item);
        this.onItemClickListener = onItemClickListener;
        itemView.setOnClickListener(this::onClick);
        addImageView.setOnClickListener(this::onAddImageClick);
    }

    @SuppressLint("SetTextI18n")
    public void bindData(Data data) {
//        name.setText(data.getModel().getName() + " from " + data.getUid());
        name.setText(data.getModel().getName());
        Picasso.get().load(data.getModel().getPhotos().get(0)).into(imageView);
    }

    private void onAddImageClick(View view) {
        if (onItemClickListener != null) onItemClickListener.onAddImageClick(getAdapterPosition());
    }


    private void onClick(View view) {
        if (onItemClickListener != null) onItemClickListener.onItemClick(getAdapterPosition());

    }

    public interface OnItemClickListener {
        void onItemClick(int pos);

        void onAddImageClick(int pos);
    }
}