package uz.N00003988.dasturkhan.ui.user.search;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Data;
import uz.N00003988.dasturkhan.room.database.Database;
import uz.N00003988.dasturkhan.ui.manager.ItemManagerFragment;
import uz.N00003988.dasturkhan.ui.manager.notification.NotificationModel;
import uz.N00003988.dasturkhan.ui.user.adapters.UserDataRecyclerViewAdapter;
import uz.N00003988.dasturkhan.ui.user.busy.UserBusyFragment;
import uz.N00003988.dasturkhan.ui.user.holders.UserDataViewHolder;
import uz.N00003988.dasturkhan.ui.user.notification.UserNotificationRecyclerViewAdapter;


public class UserSearchFragment extends Fragment implements UserDataViewHolder.OnItemClickListener {
    private String s;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private FirebaseFirestore firebaseFirestore;
    private ArrayList<Data> data;
    private ArrayList<String> keys;

    public UserSearchFragment() {
        data = new ArrayList<>();
        keys = new ArrayList<>();
        firebaseFirestore = FirebaseFirestore.getInstance();
    }

    public static UserSearchFragment instance(String s) {
        UserSearchFragment userSearchFragment = new UserSearchFragment();
        Bundle bundle = new Bundle();
        bundle.putString("TEXT", s);
        userSearchFragment.setArguments(bundle);
        return userSearchFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            s = getArguments().getString("TEXT");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_search, container, false);
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle("Search for: " + s);
        recyclerView = view.findViewById(R.id.user_list_search);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refresh();
    }

    private void refresh() {
        firebaseFirestore
                .collection("dasturkhan")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        data = new ArrayList<>();
                        keys = new ArrayList<>();
                        QuerySnapshot queryDocumentSnapshots = task.getResult();
                        for (int i = 0; i < queryDocumentSnapshots.size(); i++) {
                            keys.add(queryDocumentSnapshots.getDocuments().get(i).getId());
                            data.add(queryDocumentSnapshots.getDocuments().get(i).toObject(Data.class));
                        }
                        search();
                    }
                });
    }

    private void search() {
        ArrayList<Data> dataSearch = new ArrayList<>();
        ArrayList<String> keysSearch = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getModel().getName().trim().toLowerCase().contains(s.trim().toLowerCase())) {
                dataSearch.add(data.get(i));
                keysSearch.add(keys.get(i));
            }
        }

        recyclerView.setAdapter(new UserDataRecyclerViewAdapter(keysSearch, dataSearch, this));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(int pos) {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container2, ItemManagerFragment.instance(keys.get(pos), data.get(pos), false), "item")
                .hide(getActivity().getSupportFragmentManager().findFragmentByTag("search"))
                .addToBackStack("ITEM_USER")
                .commit();

    }

    @Override
    public void onAddImageClick(int pos) {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container2, UserBusyFragment.instance(keys.get(pos), data.get(pos)))
                .hide(getActivity().getSupportFragmentManager().findFragmentByTag("search"))
                .addToBackStack("USER_BUSY")
                .commit();
    }
}
