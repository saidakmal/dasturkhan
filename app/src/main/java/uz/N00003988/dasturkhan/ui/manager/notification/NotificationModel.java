package uz.N00003988.dasturkhan.ui.manager.notification;

import java.io.Serializable;

public class NotificationModel implements Serializable {
    private String whoFor;
    private String userId;   // user uid
    private String name;
    private String number;
    private int tableNumber;
    private String time;
    private String date;  //01.01.2019
    private String managerId; //model uid
    private String idItem;
    private String nameItem;
    private String managerAnswered; // yes/no
    private String accepted; //managerAnswered == true // yes/no

    public NotificationModel() {
    }

    public NotificationModel(String whoFor, String userId, String name, String number, int tableNumber, String time, String date, String managerId, String idItem, String nameItem, String managerAnswered, String accepted) {
        this.whoFor = whoFor;
        this.userId = userId;
        this.name = name;
        this.number = number;
        this.tableNumber = tableNumber;
        this.time = time;
        this.date = date;
        this.managerId = managerId;
        this.idItem = idItem;
        this.nameItem = nameItem;
        this.managerAnswered = managerAnswered;
        this.accepted = accepted;
    }

    public String getWhoFor() {
        return whoFor;
    }

    public void setWhoFor(String whoFor) {
        this.whoFor = whoFor;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getIdItem() {
        return idItem;
    }

    public void setIdItem(String idItem) {
        this.idItem = idItem;
    }

    public String getNameItem() {
        return nameItem;
    }

    public void setNameItem(String nameItem) {
        this.nameItem = nameItem;
    }

    public String getManagerAnswered() {
        return managerAnswered;
    }

    public void setManagerAnswered(String managerAnswered) {
        this.managerAnswered = managerAnswered;
    }

    public String getAccepted() {
        return accepted;
    }

    public void setAccepted(String accepted) {
        this.accepted = accepted;
    }
}
