package uz.N00003988.dasturkhan.ui.user.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Data;
import uz.N00003988.dasturkhan.ui.main.MainActivity;
import uz.N00003988.dasturkhan.ui.main.fragments.UserFragment;
import uz.N00003988.dasturkhan.ui.user.adapters.UserCategoryRecyclerViewAdapter;
import uz.N00003988.dasturkhan.ui.user.holders.UserCategoryViewHolder;
import uz.N00003988.dasturkhan.ui.user.holders.UserDataViewHolder;
import uz.N00003988.dasturkhan.ui.user.adapters.UserDataRecyclerViewAdapter;
import uz.N00003988.dasturkhan.ui.user.model.CategoryData;


public class UserCategoryFragment extends Fragment implements UserCategoryViewHolder.OnItemClickListener {
    private FirebaseFirestore firebaseFirestore;
    private ArrayList<CategoryData> categoryData;
    private int[] products = new int[7];
    private RecyclerView recyclerView;

    public UserCategoryFragment() {
        categoryData = new ArrayList<>();
        firebaseFirestore = FirebaseFirestore.getInstance();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_category, container, false);
        recyclerView = view.findViewById(R.id.user_category_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadProductsCount();
    }

    private void loadProductsCount() {
        CollectionReference cr = firebaseFirestore.collection("dasturkhan");
        cr.get().addOnCompleteListener(task -> products[0] = task.isSuccessful() ? task.getResult().size() : 0);
        cr.whereEqualTo("category", "Fast food").get()
                .addOnCompleteListener(task -> {
                    products[1] = task.isSuccessful() ? task.getResult().size() : 0;
                    cr.whereEqualTo("category", "Milliy Taomlar").get()
                            .addOnCompleteListener(task1 -> {
                                products[2] = task1.isSuccessful() ? task1.getResult().size() : 0;
                                cr.whereEqualTo("category", "Choyxona").get()
                                        .addOnCompleteListener(task2 -> {
                                            products[3] = task2.isSuccessful() ? task2.getResult().size() : 0;
                                            cr.whereEqualTo("category", "Breakfast").get()
                                                    .addOnCompleteListener(task3 -> {
                                                        products[4] = task3.isSuccessful() ? task3.getResult().size() : 0;
                                                        cr.whereEqualTo("category", "Chinese dishes").get()
                                                                .addOnCompleteListener(task4 -> {
                                                                    products[5] = task4.isSuccessful() ? task4.getResult().size() : 0;
                                                                    cr.whereEqualTo("category", "Osh").get()
                                                                            .addOnCompleteListener(task5 -> {
                                                                                products[6] = task5.isSuccessful() ? task5.getResult().size() : 0;
                                                                                loadData();
                                                                            });
                                                                });

                                                    });

                                        });

                            });
                });

    }

    private void loadData() {
        categoryData.add(new CategoryData("All", products[0], R.drawable.all));
        categoryData.add(new CategoryData("Fast food", products[1], R.drawable.fastfood));
        categoryData.add(new CategoryData("Milliy Taomlar", products[2], R.drawable.milliy));
        categoryData.add(new CategoryData("Choyxona", products[3], R.drawable.choyxona));
        categoryData.add(new CategoryData("Breakfast", products[4], R.drawable.breakfast));
        categoryData.add(new CategoryData("Chinese dishes", products[5], R.drawable.china));
        categoryData.add(new CategoryData("Osh", products[6], R.drawable.osh));
        recyclerView.setAdapter(new UserCategoryRecyclerViewAdapter(categoryData, this));
    }

    private void refresh() {
//        firebaseFirestore
//                .collection("dasturkhan")
//                .whereEqualTo("category", "Fast food")
//                .get()
//                .addOnCompleteListener(task -> {
//                    if (task.isSuccessful()) {
//                        data = new ArrayList<>();
//                        QuerySnapshot queryDocumentSnapshots = task.getResult();
//                        for (int i = 0; i < queryDocumentSnapshots.size(); i++) {
//                            data.add(queryDocumentSnapshots.getDocuments().get(i).toObject(Data.class));
//                        }
//                        recyclerView.setAdapter(new UserDataRecyclerViewAdapter(data, this));
//                    }
//                });
    }

    @Override
    public void onItemClick(int pos) {
        if (pos == 0) {
            ((UserFragment) getParentFragment()).viewPager.setCurrentItem(0);
        } else {
            getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container2, UserCategoriesItemFragment.instance(categoryData.get(pos).getCategory()))
                    .hide(getActivity().getSupportFragmentManager().findFragmentByTag("1"))
                    .addToBackStack("CATEGORY_ITEM_FRAGMENT")
                    .commit();
        }

    }

}
