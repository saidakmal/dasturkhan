package uz.N00003988.dasturkhan.ui.user.holders;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Data;
import uz.N00003988.dasturkhan.room.database.Database;
import uz.N00003988.dasturkhan.room.model.FavoriteModel;

public class UserDataViewHolder extends RecyclerView.ViewHolder {
    private TextView name;
    private TextView category;
    private ImageView imageView;
    private ImageView addBusyImage;
    private ImageView favouriteImage;
    private AppCompatRatingBar ratingView;
    private OnItemClickListener onItemClickListener;
    private OnNotifyListener onNotifyListener;

    public UserDataViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener, OnNotifyListener onNotifyListener) {
        super(itemView);
        this.onItemClickListener = onItemClickListener;
        this.onNotifyListener = onNotifyListener;
        name = itemView.findViewById(R.id.name_item_user);
        category = itemView.findViewById(R.id.category_item_user);
        imageView = itemView.findViewById(R.id.image_view_item_user_data);
        addBusyImage = itemView.findViewById(R.id.image_busy_user);
        favouriteImage = itemView.findViewById(R.id.image_favourite_user);
        ratingView = itemView.findViewById(R.id.rating_user_data);
        addBusyImage.setOnClickListener(this::onBusyAddClick);
        itemView.setOnClickListener(this::onClick);
        favouriteImage.setOnClickListener(v -> onNotifyListener.notify(getAdapterPosition()));
    }

    @SuppressLint("SetTextI18n")
    public void bindData(String key, Data data) {
        name.setText(data.getModel().getName());
        category.setText(data.getCategory());
        Picasso.get().load(data.getModel().getPhotos().get(0)).into(imageView);
        int size = data.getModel().getRating().size();
        float rating = 0;
        int sum = 0;
        for (int i = 0; i < size; i++) {
            sum += data.getModel().getRating().get(i).getMark();
        }
        if (size != 0) {
            rating = (float) sum / (float) size;
        }
        ratingView.setRating(rating);
        if (Database.getDatabase().getFavoriteDao().isFavourite(key) != null)
            favouriteImage.setImageResource(R.drawable.ic_favorite_black_24dp);
        else
            favouriteImage.setImageResource(R.drawable.ic_favorite_border_black_24dp);


    }

    private void onClick(View v) {
        if (onItemClickListener != null) {
            onItemClickListener.onItemClick(getAdapterPosition());
        }

    }

    private void onBusyAddClick(View v) {
        if (onItemClickListener != null) {
            onItemClickListener.onAddImageClick(getAdapterPosition());
        }

    }

    public interface OnNotifyListener {
        void notify(int pos);
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);

        void onAddImageClick(int pos);

    }


}