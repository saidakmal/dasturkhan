package uz.N00003988.dasturkhan.ui.login;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.ui.main.MainActivity;

public class LoginActivity extends AppCompatActivity {
    private String number;
    private EditText numberET;
    private EditText smsET;
    private Button next;
    private Button ok;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    private String verId;


    @Override
    protected void onStart() {
        super.onStart();
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        numberET = findViewById(R.id.et_number);
        smsET = findViewById(R.id.et_sms);
        next = findViewById(R.id.bt_next);
        ok = findViewById(R.id.bt_ok);

        ok.setVisibility(View.GONE);
        smsET.setVisibility(View.GONE);
        next.setVisibility(View.VISIBLE);
        numberET.setVisibility(View.VISIBLE);

        createCallBacks();

        next.setOnClickListener(v -> {
            number = numberET.getText().toString();
            Toast.makeText(this, "" + number, Toast.LENGTH_SHORT).show();
            verifyPhoneNumber();

            ok.setVisibility(View.VISIBLE);
            smsET.setVisibility(View.VISIBLE);
            next.setVisibility(View.GONE);
            numberET.setVisibility(View.GONE);
        });

        ok.setOnClickListener(v -> {
            String sms = smsET.getText().toString();
            signIn(PhoneAuthProvider.getCredential(verId, sms));
        });


    }

    private void verifyPhoneNumber() {
        PhoneAuthProvider
                .getInstance()
                .verifyPhoneNumber(
                        number,
                        70,
                        TimeUnit.SECONDS,
                        this,
                        callbacks
                );
    }

    private void createCallBacks() {
        callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                Toast.makeText(LoginActivity.this, "onVerificationCompleted " + phoneAuthCredential.getSmsCode(), Toast.LENGTH_SHORT).show();
                if (phoneAuthCredential.getSmsCode() != null) {
                    smsET.setText(phoneAuthCredential.getSmsCode());
                }
                signIn(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(LoginActivity.this, "onVerificationFailed", Toast.LENGTH_SHORT).show();
                Log.d("MRX",e.getMessage());
            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                verId = s;
            }

            @Override
            public void onCodeAutoRetrievalTimeOut(String s) {
                super.onCodeAutoRetrievalTimeOut(s);
                Toast.makeText(LoginActivity.this, "onCodeAutoRetrievalTimeOut", Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void signIn(PhoneAuthCredential credential) {
        FirebaseAuth.getInstance()
                .signInWithCredential(credential)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Toast.makeText(LoginActivity.this, "Sign in successful", Toast.LENGTH_SHORT).show();
                        updateUI();
                    } else {
                        Toast.makeText(LoginActivity.this, "Failed Sign in", Toast.LENGTH_SHORT).show();

                    }
                });

    }

    private void updateUI() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

}
