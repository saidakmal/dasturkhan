package uz.N00003988.dasturkhan.ui.manager.table;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Busy;
import uz.N00003988.dasturkhan.model.Data;

public class TableRecyclerViewAdapter extends RecyclerView.Adapter<TableRecyclerViewHolder> {
    private Data data;
    private Busy busy;
    private TableRecyclerViewHolder.OnItemClickListener onItemClickListener;
    private boolean isManager;

    public TableRecyclerViewAdapter(boolean isManager, Data data, Busy busy, TableRecyclerViewHolder.OnItemClickListener onItemClickListener) {
        this.data = data;
        this.busy = busy;
        this.isManager = isManager;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public TableRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new TableRecyclerViewHolder(
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_table_layout, viewGroup, false),
                onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull TableRecyclerViewHolder tableRecyclerViewHolder, int i) {
        boolean isBusy = false;
        int index = -1;
        if (busy != null) {
            for (int j = 0; j < busy.getTables().size(); j++) {
                if (busy.getTables().get(j).getTableNumber() == data.getModel().getTableNumbers().get(i)) {
                    isBusy = true;
                    index = j;
                }
            }
        }
        if (index != -1) {
            tableRecyclerViewHolder.bindData(isManager, data.getModel().getTableNumbers().get(i), isBusy, busy.getTables().get(index));
        } else {
            tableRecyclerViewHolder.bindData(isManager, data.getModel().getTableNumbers().get(i), isBusy, null);
        }
    }

    @Override
    public int getItemCount() {
        return data.getModel().getTableNumbers().size();
    }
}
