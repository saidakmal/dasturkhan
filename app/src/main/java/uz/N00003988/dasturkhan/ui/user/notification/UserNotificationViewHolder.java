package uz.N00003988.dasturkhan.ui.user.notification;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.ui.manager.notification.NotificationModel;

public class UserNotificationViewHolder extends RecyclerView.ViewHolder {
    private TextView name;
    private TextView info;
    private TextView answer;
    private Context context;


    public UserNotificationViewHolder(@NonNull View itemView) {
        super(itemView);
        context = itemView.getContext();
        name = itemView.findViewById(R.id.notif_item_name_user);
        info = itemView.findViewById(R.id.notif_date_and_time_table_user);
        answer = itemView.findViewById(R.id.notif_answer_user);
    }

    @SuppressLint("SetTextI18n")
    public void bindData(NotificationModel model) {
        name.setText(model.getNameItem());
        info.setText(model.getDate() + " " + model.getTime() + "  (" + model.getTableNumber() + "-table)");
        if (model.getManagerAnswered().equals("yes")) {
            if (model.getAccepted().equals("yes")) {
                answer.setText("ACCEPTED");
            } else {
                answer.setText("CANCELED");
            }
            answer.setTextColor(context.getResources().getColor(R.color.colorAccent));
        } else {
            answer.setText("Not answered");
        }
    }
}
