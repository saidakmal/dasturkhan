package uz.N00003988.dasturkhan.ui.user.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Data;
import uz.N00003988.dasturkhan.room.database.Database;
import uz.N00003988.dasturkhan.room.model.FavoriteModel;
import uz.N00003988.dasturkhan.ui.user.holders.UserDataViewHolder;

public class UserDataRecyclerViewAdapter extends RecyclerView.Adapter<UserDataViewHolder> {
    private ArrayList<Data> data;
    private ArrayList<String> keys;
    private UserDataViewHolder.OnItemClickListener onItemClickListener;

    public UserDataRecyclerViewAdapter(ArrayList<String> keys, ArrayList<Data> data, UserDataViewHolder.OnItemClickListener onItemClickListener) {
        this.data = data;
        this.keys = keys;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public UserDataViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new UserDataViewHolder(
                LayoutInflater
                        .from(viewGroup.getContext())
                        .inflate(R.layout.item_user_data_layout, viewGroup, false),
                onItemClickListener,
                pos -> {
                    FavoriteModel favoriteModel = new FavoriteModel(keys.get(pos), 1);
                    if (Database.getDatabase().getFavoriteDao().isFavourite(keys.get(pos)) != null) {
                        Database.getDatabase().getFavoriteDao().deleteData(favoriteModel);
                    } else {
                        Database.getDatabase().getFavoriteDao().insertFavoriteData(favoriteModel);
                    }

                    notifyItemChanged(pos);
                }
        );
    }

    @Override
    public void onBindViewHolder(@NonNull UserDataViewHolder userDataViewHolder, int i) {
        userDataViewHolder.bindData(keys.get(i), data.get(i));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


}
