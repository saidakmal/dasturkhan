package uz.N00003988.dasturkhan.ui.main;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;


import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.map.MapFragment;
import uz.N00003988.dasturkhan.settings.Settings;
import uz.N00003988.dasturkhan.ui.main.fragments.ManagerFragment;
import uz.N00003988.dasturkhan.ui.main.fragments.UserFragment;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private FirebaseUser firebaseUser;
    private BottomNavigationView bnv;
    private final Fragment fragment1 = new UserFragment();
    private final Fragment fragment2 = new ManagerFragment();
    private Fragment active = fragment1;
    public static boolean isAppRunning;
    private int MY_PERMISSIONS_REQUEST_LOCATION = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                MY_PERMISSIONS_REQUEST_LOCATION);
        Log.d("COMPLETE_SET_TOKEN", Settings.getSettings().getToken());
        if (Settings.getSettings().getToken().equals("tokenn")) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    InstanceIdResult instanceIdResult = task.getResult();
                    String token = instanceIdResult.getToken();
                    Settings.getSettings().setToken(token);
                    Log.d("COMPLETE_SET_TOKEN", token);

                }
            });

        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        String channelId = "1";
        String channel2 = "2";

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channelId,
                    "Channel 1", NotificationManager.IMPORTANCE_HIGH);

            notificationChannel.setDescription("This is BNT");
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setShowBadge(true);
            notificationManager.createNotificationChannel(notificationChannel);

            NotificationChannel notificationChannel2 = new NotificationChannel(channel2,
                    "Channel 2", NotificationManager.IMPORTANCE_MIN);

            notificationChannel.setDescription("This is bTV");
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setShowBadge(true);
            notificationManager.createNotificationChannel(notificationChannel2);

        }

        FirebaseMessaging.getInstance().subscribeToTopic(FirebaseAuth.getInstance().getCurrentUser().getUid());
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        bnv = findViewById(R.id.bnv);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, fragment2, "2")
                .hide(fragment2)
                .commit();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, fragment1, "1")
                .commit();
        bnv.setOnNavigationItemSelectedListener(this);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.user:
                getSupportFragmentManager()
                        .beginTransaction()
                        .hide(active)
                        .show(fragment1)
                        .commit();
                active = fragment1;
                return true;


            case R.id.manager:
                getSupportFragmentManager()
                        .beginTransaction()
                        .hide(active)
                        .show(fragment2)
                        .commit();
                active = fragment2;
                return true;
        }
        return false;
    }

    private void signOut() {
        FirebaseAuth.getInstance().signOut();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
