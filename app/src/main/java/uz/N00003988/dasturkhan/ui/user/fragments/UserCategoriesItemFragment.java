package uz.N00003988.dasturkhan.ui.user.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Data;
import uz.N00003988.dasturkhan.ui.manager.ItemManagerFragment;
import uz.N00003988.dasturkhan.ui.user.adapters.UserDataRecyclerViewAdapter;
import uz.N00003988.dasturkhan.ui.user.busy.UserBusyFragment;
import uz.N00003988.dasturkhan.ui.user.holders.UserDataViewHolder;

public class UserCategoriesItemFragment extends Fragment implements UserDataViewHolder.OnItemClickListener {
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private FirebaseFirestore firebaseFirestore;
    private static final String CATEGORY = "CATEGORY";
    private String category;
    private ArrayList<String> keys;
    private ArrayList<Data> data;


    public UserCategoriesItemFragment() {
        // Required empty public constructor
    }

    public static UserCategoriesItemFragment instance(String category) {
        UserCategoriesItemFragment fragment = new UserCategoriesItemFragment();
        Bundle args = new Bundle();
        args.putSerializable(CATEGORY, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        firebaseFirestore = FirebaseFirestore.getInstance();
        if (getArguments() != null) {
            category = (String) getArguments().getSerializable(CATEGORY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_categories_item, container, false);
        recyclerView = view.findViewById(R.id.categories_item_list);
        toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refresh();

    }

    private void refresh() {
        firebaseFirestore.collection("dasturkhan").whereEqualTo("category", category).get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        data = new ArrayList<>();
                        keys = new ArrayList<>();
                        QuerySnapshot queryDocumentSnapshots = task.getResult();
                        for (int i = 0; i < queryDocumentSnapshots.size(); i++) {
                            data.add(queryDocumentSnapshots.getDocuments().get(i).toObject(Data.class));
                            keys.add(queryDocumentSnapshots.getDocuments().get(i).getId());
                        }
                        recyclerView.setAdapter(new UserDataRecyclerViewAdapter(keys, data, this));
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(int pos) {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container2, ItemManagerFragment.instance(keys.get(pos), data.get(pos), false), "item")
                .addToBackStack("ITEM_USER")
                .commit();

        Toast.makeText(getContext(), "" + pos, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddImageClick(int pos) {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container2, UserBusyFragment.instance(keys.get(pos), data.get(pos)))
                .addToBackStack("USER_BUSY")
                .commit();
    }

}
