package uz.N00003988.dasturkhan.ui.manager.add;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import uz.N00003988.dasturkhan.R;

public class AddPhotosViewHolder extends RecyclerView.ViewHolder {
    private ImageView imageView;
    private ImageView imageViewRemove;
    private OnRemoveListener onRemoveListener;

    public AddPhotosViewHolder(@NonNull View itemView, OnRemoveListener onRemoveListener) {
        super(itemView);
        this.onRemoveListener = onRemoveListener;
        imageViewRemove = itemView.findViewById(R.id.remove_photos);
        imageView = itemView.findViewById(R.id.image_add);
        imageViewRemove.setOnClickListener(v -> {
            onRemoveListener.OnRemove(getAdapterPosition());
        });
    }

    public void bindData(String url) {
        Picasso.get().load(url).into(imageView);
    }

    public interface OnRemoveListener {
        void OnRemove(int pos);
    }
}
