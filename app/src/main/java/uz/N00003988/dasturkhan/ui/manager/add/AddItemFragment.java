package uz.N00003988.dasturkhan.ui.manager.add;


import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Busy;
import uz.N00003988.dasturkhan.model.Data;
import uz.N00003988.dasturkhan.model.Model;
import uz.N00003988.dasturkhan.model.Rating;
import uz.N00003988.dasturkhan.model.TableModel;
import uz.N00003988.dasturkhan.ui.manager.ItemManagerFragment;


public class AddItemFragment extends Fragment implements AddPhotosViewHolder.OnRemoveListener {
    private int GALLERY = 1001;
    public static final String KEY = "KEY";
    public static final String DATA = "DATA";
    public static final String IS_EDIT = "IS_EDIT";
    private String key;
    private Data data;
    private boolean isEdit;
    private Toolbar toolbar;
    private EditText nameEditText;
    private EditText addressEditText;
    private AppCompatSpinner appCompatSpinner;
    private RecyclerView recyclerView;
    private ImageView imageAdd;
    private FirebaseFirestore firebaseFirestore;
    private Button buttonAdd;
    private String number;
    private String uid;
    private String name = "safas";
    private String address = "safasfd";
    private ArrayList<String> categories = new ArrayList<>();
    private int posCategory = 0;
    private File file;
    private Uri uri;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private ArrayList<String> photos;
    private Marker marker = null;
    private GeoPoint geoPointAdd;
    private GeoPoint geoPoint;
    private ImageView locationAddImage;
    private TextView locationTextView;


    public AddItemFragment() {
        // Required empty public constructor
    }

    public static AddItemFragment instance(String key, Data data, boolean isEdit) {
        AddItemFragment addItemFragment = new AddItemFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY, key);
        bundle.putSerializable(DATA, data);
        bundle.putSerializable(IS_EDIT, isEdit); //true
        addItemFragment.setArguments(bundle);
        return addItemFragment;
    }

    public static AddItemFragment instance(boolean isEdit) {
        AddItemFragment addItemFragment = new AddItemFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(IS_EDIT, isEdit); //false
        addItemFragment.setArguments(bundle);
        return addItemFragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY && data != null) {
            uri = data.getData();
            String path = getRealPathFromURI(uri);
            file = new File(path);
            uploadImage(uri);
        }

    }

    public void uploadImage(Uri uri) {
        storageReference = storage.getReference().child("images/" + uri.getPath());
        UploadTask uploadTask = storageReference.putFile(uri);
        uploadTask.
                continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    return storageReference.getDownloadUrl();
                }).
                addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        onSuccessUploadImage(task.getResult().toString());
                    }
                });
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = new String[]{MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri,
                proj, // WHERE clause selection arguments (none)
                null, null, null);// Which columns to return
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void onSuccessUploadImage(String url) {
        Toast.makeText(getContext(), "url refresh " + url, Toast.LENGTH_SHORT).show();
        if (isEdit) {
            data.getModel().getPhotos().add(url);
        } else {
            photos.add(url);
        }
        refresh();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        number = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber();
        uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        firebaseFirestore = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        if (getArguments() != null) {
            if (getArguments().getSerializable(KEY) != null) {
                key = (String) getArguments().getSerializable(KEY);
                data = (Data) getArguments().getSerializable(DATA);
                isEdit = (boolean) getArguments().getSerializable(IS_EDIT);
            } else {
                photos = new ArrayList<>();
                isEdit = (boolean) getArguments().getSerializable(IS_EDIT);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_item, container, false);
        toolbar = view.findViewById(R.id.toolbar);
        nameEditText = view.findViewById(R.id.name_add);
        addressEditText = view.findViewById(R.id.address_add);
        appCompatSpinner = view.findViewById(R.id.spinner_add);
        imageAdd = view.findViewById(R.id.image_add);
        locationTextView = view.findViewById(R.id.location_text);
        locationAddImage = view.findViewById(R.id.location_add);
        buttonAdd = view.findViewById(R.id.bt_add);
        recyclerView = view.findViewById(R.id.list_photos_add);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        loadCategories();
        ArrayAdapter<CharSequence> stringArrayAdapter = ArrayAdapter.createFromResource(getContext(), R.array.Categories, android.R.layout.simple_spinner_item);
        stringArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        appCompatSpinner.setAdapter(stringArrayAdapter);
        appCompatSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                posCategory = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//                appCompatSpinner.setSelection(1);

        if (isEdit) {
            recyclerView.setAdapter(new AddPhotoRecyclerViewAdapter(data.getModel().getPhotos(), this));
            buttonAdd.setText("Edit");
            nameEditText.setText(data.getModel().getName());
            name = nameEditText.getText().toString();
            addressEditText.setText(data.getModel().getAddress());
            address = addressEditText.getText().toString();
            appCompatSpinner.setSelection(categories.indexOf(data.getCategory()));
            geoPoint = data.getLocation();
            locationTextView.setText("Location: " + geoPoint.getLatitude() + "," + geoPoint.getLongitude());
        } else {
            geoPoint = null;
            locationTextView.setText("Location isn't available");
            recyclerView.setAdapter(new AddPhotoRecyclerViewAdapter(photos, this));
            buttonAdd.setText("Add");

        }

        buttonAdd.setOnClickListener(v -> {
            if (isEdit) {
                edit();
            } else {
                add();
            }
        });

        imageAdd.setOnClickListener(v -> {
            choosePhotoFromGallary();
            Toast.makeText(getContext(), "add photos", Toast.LENGTH_SHORT).show();
        });

        locationAddImage.setOnClickListener(v -> {
            showMap();
            Toast.makeText(getContext(), "add location", Toast.LENGTH_SHORT).show();
        });

        nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                name = nameEditText.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        addressEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                address = addressEditText.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }


    private void loadCategories() {
        categories.add("Fast food");
        categories.add("Milliy Taomlar");
        categories.add("Choyxona");
        categories.add("Breakfast");
        categories.add("Chinese dishes");
        categories.add("Osh");
    }

    private void delete() {
        firebaseFirestore.collection("dasturkhan").document(key).delete().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                refresh();
            }
        });
    }

    private void edit() {
        if (data.getModel().getPhotos().size() > 0) {
            Toast.makeText(getContext(), "Please wait...", Toast.LENGTH_SHORT).show();
            data.getModel().setName(name);
            data.getModel().setAddress(address);
            data.setCategory(categories.get(posCategory));
            data.setLocation(geoPoint);
            firebaseFirestore.collection("dasturkhan").document(key).set(data).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    refresh();
                    getActivity().onBackPressed();
                }
            });
        } else {
            Toast.makeText(getContext(), "Add a photo plese", Toast.LENGTH_SHORT).show();
        }
    }

    private void add() {
        if (photos.size() > 0 && geoPoint != null) {
            Toast.makeText(getContext(), "Please wait...", Toast.LENGTH_SHORT).show();

            // name address category photos

            ArrayList<Busy> busy = new ArrayList<>();
            Model model;

            ArrayList<TableModel> tables = new ArrayList<>();
//        tables.add(new TableModel("name", "Number", 1, "18:30"));

//        busy.add(new Busy("03.02.2019", tables));

            ArrayList<Rating> rating = new ArrayList<>();
//        rating.add(new Rating("afadgsg", 2, "asfaf", "asfa afs asf"));

            ArrayList<Integer> tableNumbers = new ArrayList<>();
            tableNumbers.add(1);

            ArrayList<String> photos = new ArrayList<>();
            photos.addAll(this.photos);
//        photos.add("url 1 add");

            model = new Model(
                    name,
                    number,
                    address,
                    rating,
                    tableNumbers,
                    photos
            );
            Data dataModel = new Data(geoPoint, busy, model, categories.get(posCategory), uid);
            firebaseFirestore.collection("dasturkhan").add(dataModel).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    refresh();
                    getActivity().onBackPressed();
                }
            });
        } else {
            Toast.makeText(getContext(), "Add a photo and location please", Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressLint("SetTextI18n")
    private void showMap() {
        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        alertDialog.setTitle("Point Location");
        alertDialog.setIcon(R.drawable.ic_add_location_black_24dp);
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View promptView = layoutInflater.inflate(R.layout.dialog_map_add_layout, null);
        alertDialog.setView(promptView);

        MapView mMapView = (MapView) promptView.findViewById(R.id.schedule_map);
        MapsInitializer.initialize(getContext());

        mMapView.onCreate(alertDialog.onSaveInstanceState());
        mMapView.onResume();


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap googleMap) {
                if (isEdit) {
                    marker = googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(geoPoint.getLatitude(), geoPoint.getLongitude()))
                            .title("location"));


                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(geoPoint.getLatitude(), geoPoint.getLongitude()), 15));
                } else {
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(41.31, 69.28), 15));

                }
//                googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                googleMap.setMyLocationEnabled(true);
                googleMap.setOnMapClickListener(latLng -> {
                    if (marker != null)
                        marker.remove();
                    marker = googleMap.addMarker(new MarkerOptions().position(latLng).title("location"));
                    geoPointAdd = new GeoPoint(latLng.latitude, latLng.longitude);
                });
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Set location", (dialog, which) -> {
            if (geoPointAdd != null) {
                geoPoint = geoPointAdd;
                locationTextView.setText("Location: " + geoPoint.getLatitude() + "," + geoPoint.getLongitude());
            } else {
                Toast.makeText(getContext(), "not found locaton", Toast.LENGTH_SHORT).show();
            }

            dialog.cancel();
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", (dialog, i) -> {
            Toast.makeText(getContext(), "Canceled!", Toast.LENGTH_SHORT).show();
            dialog.cancel();
        });

        alertDialog.show();


    }

    private void refresh() {
        if (isEdit) {
            recyclerView.setAdapter(new AddPhotoRecyclerViewAdapter(data.getModel().getPhotos(), this));

        } else {
            recyclerView.setAdapter(new AddPhotoRecyclerViewAdapter(photos, this));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnRemove(int pos) {
        if (isEdit) {
            data.getModel().getPhotos().remove(pos);
            recyclerView.setAdapter(new AddPhotoRecyclerViewAdapter(data.getModel().getPhotos(), this));

        } else {
            photos.remove(pos);
            recyclerView.setAdapter(new AddPhotoRecyclerViewAdapter(photos, this));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (isEdit) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .show(getActivity().getSupportFragmentManager().findFragmentByTag("2"))
                    .commit();
        }
    }
}
