package uz.N00003988.dasturkhan.ui.user.notification;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.ui.manager.notification.NotificationModel;

public class UserNotificationRecyclerViewAdapter extends RecyclerView.Adapter<UserNotificationViewHolder> {
    private ArrayList<NotificationModel> notificationModels;

    public UserNotificationRecyclerViewAdapter(ArrayList<NotificationModel> notificationModels) {
        this.notificationModels = notificationModels;
    }

    @NonNull
    @Override
    public UserNotificationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new UserNotificationViewHolder(
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_notification_user_layout, viewGroup, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull UserNotificationViewHolder userNotificationViewHolder, int i) {
        userNotificationViewHolder.bindData(notificationModels.get(i));
    }

    @Override
    public int getItemCount() {
        return notificationModels.size();
    }
}
