package uz.N00003988.dasturkhan.ui.user.notification;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.ui.manager.notification.NotificationModel;

public class UserNotificationFragment extends Fragment {
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private ArrayList<NotificationModel> notificationModels;
    private ArrayList<String> keys;

    public UserNotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        notificationModels = new ArrayList<>();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_notification, container, false);
        toolbar = view.findViewById(R.id.toolbar);
        recyclerView = view.findViewById(R.id.user_list_notif);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refresh();

    }

    private void refresh() {
        FirebaseFirestore.getInstance()
                .collection("notification")
                .whereEqualTo("userId", FirebaseAuth.getInstance().getCurrentUser().getUid())
                .get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                notificationModels = new ArrayList<>();
                keys = new ArrayList<>();
                QuerySnapshot queryDocumentSnapshots = task.getResult();
                for (int i = 0; i < queryDocumentSnapshots.size(); i++) {
                    notificationModels.add(queryDocumentSnapshots.getDocuments().get(i).toObject(NotificationModel.class));
                    keys.add(queryDocumentSnapshots.getDocuments().get(i).getId());
                }
                recyclerView.setAdapter(new UserNotificationRecyclerViewAdapter(notificationModels));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
