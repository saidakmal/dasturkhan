package uz.N00003988.dasturkhan.ui.manager.notification;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.JsonParser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Busy;
import uz.N00003988.dasturkhan.model.Data;
import uz.N00003988.dasturkhan.model.TableModel;
import uz.N00003988.dasturkhan.retrofit.ApiInterface;
import uz.N00003988.dasturkhan.retrofit.ApiRetrofit;
import uz.N00003988.dasturkhan.retrofit.models.ResponseData;
import uz.N00003988.dasturkhan.retrofit.models.UserMessageData;
import uz.N00003988.dasturkhan.retrofit.models.UserMessageModel;


public class NotificationFragment extends Fragment implements NotificationViewHolder.OnAnsweredLisnener {
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private ArrayList<NotificationModel> notificationModels;
    private ArrayList<String> keys;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");


    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        notificationModels = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        toolbar = view.findViewById(R.id.toolbar);
        recyclerView = view.findViewById(R.id.list_notif);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refresh();

    }

    private void refresh() {
        FirebaseFirestore.getInstance()
                .collection("notification")
                .whereEqualTo("managerId", FirebaseAuth.getInstance().getCurrentUser().getUid())
                .whereEqualTo("managerAnswered", "no")
                .get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                notificationModels = new ArrayList<>();
                keys = new ArrayList<>();
                QuerySnapshot queryDocumentSnapshots = task.getResult();
                for (int i = 0; i < queryDocumentSnapshots.size(); i++) {
                    notificationModels.add(queryDocumentSnapshots.getDocuments().get(i).toObject(NotificationModel.class));
                    keys.add(queryDocumentSnapshots.getDocuments().get(i).getId());
                }
                recyclerView.setAdapter(new NotificationRecyclerViewAdapter(notificationModels, this));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCancel(int pos) {
        Toast.makeText(getContext(), "Cancel", Toast.LENGTH_SHORT).show();
        String topic = notificationModels.get(pos).getUserId();
        String whoFor = "user";
        String answer = "Canceled";
        UserMessageModel data = new UserMessageModel(answer, whoFor);
        UserMessageData userMessageData = new UserMessageData(topic, data);
        notificationModels.get(pos).setManagerAnswered("yes");
        notificationModels.get(pos).setAccepted("no");
        ApiInterface apiInterface = ApiRetrofit.getRetrofit().create(ApiInterface.class);
        Call<ResponseData> call = apiInterface.sendMessage(new JsonParser().parse(userMessageData.toString()).getAsJsonObject());
        call.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful()) {
                    FirebaseFirestore.getInstance()
                            .collection("notification").document(keys.get(pos))
                            .set(notificationModels.get(pos))
                            .addOnCompleteListener(task -> {
                                refresh();
                            });
                    Toast.makeText(getContext(), "Canceled", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onAccept(int pos) {
        Toast.makeText(getContext(), "Accept... Please wait", Toast.LENGTH_SHORT).show();
        NotificationModel notificationModel = notificationModels.get(pos);
        FirebaseFirestore.getInstance().collection("dasturkhan").document(notificationModels.get(pos).getIdItem())
                .get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                String topic = notificationModels.get(pos).getUserId();
                String whoFor = "user";
                String answer = "Accepted";
                UserMessageModel dataModel = new UserMessageModel(answer, whoFor);
                UserMessageData userMessageData = new UserMessageData(topic, dataModel);
                notificationModels.get(pos).setManagerAnswered("yes");
                notificationModels.get(pos).setAccepted("yes");

                DocumentSnapshot documentSnapshot = task.getResult();
                Data data = documentSnapshot.toObject(Data.class);

                int busyPosition = -1;
                for (int i = 0; i < data.getBusy().size(); i++) {
                    if (notificationModel.getDate().equals(data.getBusy().get(i).getDate())) {
                        busyPosition = i;
                    }
                }
//
                if (busyPosition != -1) {
                    TableModel table = new TableModel(notificationModel.getName(), notificationModel.getNumber(), notificationModel.getTableNumber(), notificationModel.getTime());
                    data.getBusy().get(busyPosition).getTables().add(table);
                    FirebaseFirestore.getInstance()
                            .collection("dasturkhan")
                            .document(notificationModel.getIdItem()).set(data).addOnCompleteListener(task1 -> {
                        ApiInterface apiInterface = ApiRetrofit.getRetrofit().create(ApiInterface.class);
                        Call<ResponseData> call = apiInterface.sendMessage(new JsonParser().parse(userMessageData.toString()).getAsJsonObject());
                        call.enqueue(new Callback<ResponseData>() {
                            @Override
                            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                                if (response.isSuccessful()) {

                                    FirebaseFirestore.getInstance()
                                            .collection("notification").document(keys.get(pos))
                                            .set(notificationModels.get(pos))
                                            .addOnCompleteListener(task -> {
                                                refresh();
                                            });
                                    Toast.makeText(getContext(), "Accepted", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseData> call, Throwable t) {
                                Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
                            }
                        });
                    });
                } else {
                    // new date add
                    ArrayList<TableModel> tables = new ArrayList<>();
                    TableModel table = new TableModel(notificationModel.getName(), notificationModel.getNumber(), notificationModel.getTableNumber(), notificationModel.getTime());
                    tables.add(table);
                    Busy busy = new Busy(notificationModel.getDate(), tables);
                    data.getBusy().add(busy);
                    FirebaseFirestore.getInstance()
                            .collection("dasturkhan")
                            .document(notificationModel.getIdItem()).set(data).addOnCompleteListener(task1 -> {
                        ApiInterface apiInterface = ApiRetrofit.getRetrofit().create(ApiInterface.class);
                        Call<ResponseData> call = apiInterface.sendMessage(new JsonParser().parse(userMessageData.toString()).getAsJsonObject());
                        call.enqueue(new Callback<ResponseData>() {
                            @Override
                            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                                if (response.isSuccessful()) {

                                    FirebaseFirestore.getInstance()
                                            .collection("notification").document(keys.get(pos))
                                            .set(notificationModels.get(pos))
                                            .addOnCompleteListener(task -> {
                                                refresh();
                                            });
                                    Toast.makeText(getContext(), "Accepted", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseData> call, Throwable t) {
                                Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
                            }
                        });
                    });
                }

            }
        });
//


    }
}
