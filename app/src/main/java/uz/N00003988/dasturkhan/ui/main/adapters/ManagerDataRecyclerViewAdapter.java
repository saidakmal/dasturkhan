package uz.N00003988.dasturkhan.ui.main.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Data;
import uz.N00003988.dasturkhan.ui.main.holders.ManagerDataViewHolder;

public class ManagerDataRecyclerViewAdapter extends RecyclerView.Adapter<ManagerDataViewHolder> {
    private ArrayList<Data> data;
    private ManagerDataViewHolder.OnItemClickListener onItemClickListener;

    public ManagerDataRecyclerViewAdapter(ArrayList<Data> data, ManagerDataViewHolder.OnItemClickListener onItemClickListener) {
        this.data = data;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ManagerDataViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ManagerDataViewHolder(
                LayoutInflater
                        .from(viewGroup.getContext())
                        .inflate(R.layout.item_manager_data_layout, viewGroup, false),
                onItemClickListener
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ManagerDataViewHolder userDataViewHolder, int i) {
        userDataViewHolder.bindData(data.get(i));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


}
