package uz.N00003988.dasturkhan.ui.user.holders;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.ui.user.model.CategoryData;

public class UserCategoryViewHolder extends RecyclerView.ViewHolder {
    private ImageView imageView;
    private TextView products;
    private TextView categoryText;
    private OnItemClickListener onItemClickListener;


    public UserCategoryViewHolder(View itemView, OnItemClickListener onItemClickListener) {
        super(itemView);
        this.onItemClickListener = onItemClickListener;
        imageView = itemView.findViewById(R.id.image_category);
        products = itemView.findViewById(R.id.text_view_products);
        categoryText = itemView.findViewById(R.id.text_category);
        itemView.setOnClickListener(this::click);
    }

    @SuppressLint("SetTextI18n")
    public void bind(CategoryData category) {
        Picasso.get().load(category.getSrc()).into(imageView);
        products.setText(category.getProducts() + " Products");
        categoryText.setText(category.getCategory());
    }

    public void click(View view) {
        if (onItemClickListener != null) {
            onItemClickListener.onItemClick(getAdapterPosition());
        }

    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }
}
