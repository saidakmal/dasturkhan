package uz.N00003988.dasturkhan.ui.manager.pager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


import uz.N00003988.dasturkhan.R;


public class ItemImageFragment extends Fragment {
    public static final String URL = "URL";
    public static final String POS = "POS";
    public static final String SIZE = "SIZE";
    private String url;
    private int pos;
    private int size;
    private ImageView imageView;
    private TextView textView;


    public ItemImageFragment() {
        // Required empty public constructor
    }

    public static ItemImageFragment instance(String url, int pos, int size) {
        ItemImageFragment itemImageFragment = new ItemImageFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(URL, url);
        bundle.putSerializable(POS, pos);
        bundle.putSerializable(SIZE, size);
        itemImageFragment.setArguments(bundle);
        return itemImageFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = (String) getArguments().getSerializable(URL);
            pos = (int) getArguments().getSerializable(POS);
            size = (int) getArguments().getSerializable(SIZE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_image, container, false);
        imageView = view.findViewById(R.id.image_view_item_image_fragment);
        textView = view.findViewById(R.id.text_view_item_image_fragment);
        return view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Picasso.get().load(url).into(imageView);
        textView.setText(pos + "/" + size);
    }
}
