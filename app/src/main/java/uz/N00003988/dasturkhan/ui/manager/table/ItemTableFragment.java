package uz.N00003988.dasturkhan.ui.manager.table;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Busy;
import uz.N00003988.dasturkhan.model.Data;
import uz.N00003988.dasturkhan.model.TableModel;

public class ItemTableFragment extends Fragment implements TableRecyclerViewHolder.OnItemClickListener {
    public static final String KEY = "KEY";
    public static final String DATA = "DATA";
    private String key;
    private Data data;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private FloatingActionButton fab;
    private TextView textViewData;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private Calendar calendar = Calendar.getInstance();
    private Busy busy = null;
    int year = -1;
    int month = -1;
    int dayOfMonth = -1;
    private String nameText;
    private String numberText;
    private String timeText;
    int posBusyTable = -1;

    public ItemTableFragment() {
        // Required empty public constructor
    }

    public static ItemTableFragment instance(String key, Data data) {
        ItemTableFragment itemTableFragment = new ItemTableFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY, key);
        bundle.putSerializable(DATA, data);
        itemTableFragment.setArguments(bundle);
        return itemTableFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            key = (String) getArguments().getSerializable(KEY);
            data = (Data) getArguments().getSerializable(DATA);
            String date = simpleDateFormat.format(calendar.getTimeInMillis());
            for (int i = 0; i < data.getBusy().size(); i++) {
                if (date.equals(data.getBusy().get(i).getDate())) {
                    busy = data.getBusy().get(i);
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_table, container, false);
        toolbar = view.findViewById(R.id.toolbar);
        recyclerView = view.findViewById(R.id.recycler_view_table);
        fab = view.findViewById(R.id.fab);
        textViewData = view.findViewById(R.id.sub_text_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new TableRecyclerViewAdapter(true, data, busy, this));
        // keyga datani set qilib quyiladi
        fab.setOnClickListener(v -> Toast.makeText(getContext(), "add", Toast.LENGTH_SHORT).show());
        textViewData.setText(simpleDateFormat.format(calendar.getTimeInMillis()));

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.table_fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        } else if (item.getItemId() == R.id.calendar) {
            View v = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_calendar_layout, null, false);
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                    .setView(v)
                    .setPositiveButton("OK", (dialog, which) -> {
                        if (year != -1 && month != -1 && dayOfMonth != -1) {
                            calendar = new GregorianCalendar(year, month, dayOfMonth);
                            textViewData.setText(simpleDateFormat.format(calendar.getTimeInMillis()));
                            String date = simpleDateFormat.format(calendar.getTimeInMillis());
                            for (int i = 0; i < data.getBusy().size(); i++) {
                                if (date.equals(data.getBusy().get(i).getDate())) {
                                    busy = data.getBusy().get(i);
                                }
                            }
                            recyclerView.setAdapter(new TableRecyclerViewAdapter(true, data, busy, this));
                        }
                        dialog.cancel();
                    })
                    .setNegativeButton("Cancel", (dialog, which) -> dialog.cancel())
                    .show();

            CalendarView calendarView = alertDialog.findViewById(R.id.calendar_dialog);
            if (calendarView != null) {
                calendarView.setDate(calendar.getTimeInMillis(), false, false);
                calendarView.setOnDateChangeListener((view1, year, month, dayOfMonth) -> {
                    busy = null;
                    this.year = year;
                    this.month = month;
                    this.dayOfMonth = dayOfMonth;
                });
            }
        } else if (item.getItemId() == R.id.add_table) {
            View v = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_ask_for_add_layout, null, false);
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                    .setView(v)
                    .setTitle("Add Table")
                    .setPositiveButton("OK", (dialog, which) -> {
                        int d = data.getModel().getTableNumbers().size() + 1;
                        data.getModel().getTableNumbers().add(d);
                        String date = simpleDateFormat.format(calendar.getTimeInMillis());
                        for (int i = 0; i < data.getBusy().size(); i++) {
                            if (date.equals(data.getBusy().get(i).getDate())) {
                                busy = data.getBusy().get(i);
                            }
                        }
                        FirebaseFirestore.getInstance().collection("dasturkhan").document(key).set(data)
                                .addOnCompleteListener(task -> {
                                    recyclerView.setAdapter(new TableRecyclerViewAdapter(true, data, busy, this));
                                });
                        dialog.cancel();
                    })
                    .setNegativeButton("Cancel", (dialog, which) -> dialog.cancel())
                    .show();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .show(getActivity()
                        .getSupportFragmentManager()
                        .findFragmentByTag("2"))
                .commit();
    }

    @Override
    public void onMoreItemClick(int pos, View view) {
        boolean isTabeBusy = false;
        if (busy != null) {
            for (int i = 0; i < busy.getTables().size(); i++) {
                if (busy.getTables().get(i).getTableNumber() == (pos + 1)) {
                    isTabeBusy = true;
                    break;
                }
            }
        }
        PopupMenu popupMenu = new PopupMenu(getContext(), view);
        if (isTabeBusy) {
            popupMenu.inflate(R.menu.menu_busy_table);
        } else {
            popupMenu.inflate(R.menu.menu_set_busy_table);
        }
        popupMenu.setOnMenuItemClickListener(menuItem -> {
            int id = menuItem.getItemId();
            if (id == R.id.action_edit_table) {
                View v = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_edit_table_layout, null, false);
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                        .setView(v)
                        .setTitle("Edit table")
                        .setPositiveButton("EDIT", (dialog, which) -> {
                            if (posBusyTable != -1) {
                                int busyPosition = -1;
                                String date = simpleDateFormat.format(calendar.getTimeInMillis());
                                for (int i = 0; i < data.getBusy().size(); i++) {
                                    if (date.equals(data.getBusy().get(i).getDate())) {
                                        busy = data.getBusy().get(i);
                                        busyPosition = i;
                                    }
                                }
                                if (busyPosition != -1) {
                                    data.getBusy().get(busyPosition).getTables().get(posBusyTable).setName(nameText);
                                    data.getBusy().get(busyPosition).getTables().get(posBusyTable).setNumber(numberText);
                                    data.getBusy().get(busyPosition).getTables().get(posBusyTable).setTime(timeText);
                                    busy = data.getBusy().get(busyPosition);
                                    FirebaseFirestore.getInstance()
                                            .collection("dasturkhan")
                                            .document(key).set(data).addOnCompleteListener(task -> {
                                        posBusyTable = -1;
                                        recyclerView.setAdapter(new TableRecyclerViewAdapter(true, data, busy, this));
                                    });

                                }
                            }
                            dialog.cancel();
                        })
                        .setNegativeButton("Cancel", (dialog, which) -> dialog.cancel())
                        .show();
                EditText name = alertDialog.findViewById(R.id.name_table_busy);
                EditText number = alertDialog.findViewById(R.id.number_table_busy);
                EditText time = alertDialog.findViewById(R.id.time_table_busy);
                if (name != null && number != null && time != null) {
                    for (int i = 0; i < busy.getTables().size(); i++) {
                        if ((pos + 1) == busy.getTables().get(i).getTableNumber()) {
                            posBusyTable = i;
                        }
                    }
                    if (posBusyTable != -1) {
                        name.setText(busy.getTables().get(posBusyTable).getName());
                        number.setText(busy.getTables().get(posBusyTable).getNumber());
                        time.setText(busy.getTables().get(posBusyTable).getTime());
                        nameText = name.getText().toString();
                        numberText = number.getText().toString();
                        timeText = time.getText().toString();
                    }
                    name.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            nameText = s.toString();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });
                    number.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            numberText = s.toString();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });
                    time.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            timeText = s.toString();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });
                }


            } else if (id == R.id.action_not_busy) {
                int busyPosition = -1;
                String date = simpleDateFormat.format(calendar.getTimeInMillis());
                for (int i = 0; i < data.getBusy().size(); i++) {
                    if (date.equals(data.getBusy().get(i).getDate())) {
                        busy = data.getBusy().get(i);
                        busyPosition = i;
                    }
                }
                if (busyPosition != -1) {
                    for (int i = 0; i < busy.getTables().size(); i++) {
                        if ((pos + 1) == busy.getTables().get(i).getTableNumber()) {
                            posBusyTable = i;
                        }
                    }
                    data.getBusy().get(busyPosition).getTables().remove(posBusyTable);
                    busy = data.getBusy().get(busyPosition);
                    FirebaseFirestore.getInstance().collection("dasturkhan").document(key).set(data)
                            .addOnCompleteListener(task -> {
                                recyclerView.setAdapter(new TableRecyclerViewAdapter(true, data, busy, this));
                            });
                }


            } else if (id == R.id.action_set_busy) {
                View v = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_edit_table_layout, null, false);
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                        .setView(v)
                        .setTitle("Set busy")
                        .setPositiveButton("OK", (dialog, which) -> {
                            int busyPosition = -1;
                            String date = simpleDateFormat.format(calendar.getTimeInMillis());
                            for (int i = 0; i < data.getBusy().size(); i++) {
                                if (date.equals(data.getBusy().get(i).getDate())) {
                                    busy = data.getBusy().get(i);
                                    busyPosition = i;
                                }
                            }

                            if (busyPosition != -1) {
                                TableModel table = new TableModel(nameText, numberText, pos + 1, timeText);
                                data.getBusy().get(busyPosition).getTables().add(table);
                                busy = data.getBusy().get(busyPosition);
                                FirebaseFirestore.getInstance()
                                        .collection("dasturkhan")
                                        .document(key).set(data).addOnCompleteListener(task -> {
                                    recyclerView.setAdapter(new TableRecyclerViewAdapter(true, data, busy, this));

                                });

                            } else {
                                // new date add
                                ArrayList<TableModel> tables = new ArrayList<>();
                                TableModel table = new TableModel(nameText, numberText, pos + 1, timeText);
                                tables.add(table);
                                Busy busy = new Busy(simpleDateFormat.format(calendar.getTimeInMillis()), tables);
                                this.busy = busy;
                                data.getBusy().add(busy);
                                FirebaseFirestore.getInstance()
                                        .collection("dasturkhan")
                                        .document(key).set(data).addOnCompleteListener(task -> {
                                    recyclerView.setAdapter(new TableRecyclerViewAdapter(true, data, busy, this));
                                });
                            }

                            dialog.cancel();
                        })
                        .setNegativeButton("Cancel", (dialog, which) -> dialog.cancel())
                        .show();
                EditText name = alertDialog.findViewById(R.id.name_table_busy);
                EditText number = alertDialog.findViewById(R.id.number_table_busy);
                EditText time = alertDialog.findViewById(R.id.time_table_busy);
                if (name != null && number != null && time != null) {
                    nameText = name.getText().toString();
                    numberText = number.getText().toString();
                    timeText = time.getText().toString();
                    name.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            nameText = s.toString();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });
                    number.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            numberText = s.toString();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });
                    time.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            timeText = s.toString();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });
                }
            }
            return true;
        });
        popupMenu.show();
    }
}
