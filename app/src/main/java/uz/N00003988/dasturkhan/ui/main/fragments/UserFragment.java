package uz.N00003988.dasturkhan.ui.main.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.map.MapFragment;
import uz.N00003988.dasturkhan.ui.main.adapters.UserViewPagerAdapter;
import uz.N00003988.dasturkhan.ui.user.notification.UserNotificationFragment;
import uz.N00003988.dasturkhan.ui.user.search.UserSearchFragment;

public class UserFragment extends Fragment {
    private TabLayout tabLayout;
    public ViewPager viewPager;
    private Toolbar toolbar;

    //
    public UserFragment() {
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
//            if (onRefreshListener != null) {
//                onRefreshListener.onRefresh();
//            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user, container, false);
        tabLayout = view.findViewById(R.id.tabs_user);
        viewPager = view.findViewById(R.id.pager_user);
        toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        return view;
    }

    //
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager.setAdapter(new UserViewPagerAdapter(getChildFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container2, UserSearchFragment.instance(s), "search")
                        .hide(getActivity().getSupportFragmentManager().findFragmentByTag("1"))
                        .addToBackStack("USER_SEARCH")
                        .commit();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_out: {
                FirebaseAuth.getInstance().signOut();
                getActivity().finish();
                break;
            }
            case R.id.action_message: {
                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container2, new UserNotificationFragment())
                        .hide(getActivity().getSupportFragmentManager().findFragmentByTag("1"))
                        .addToBackStack("USER_NOTIFICATION_FRAGMENT")
                        .commit();
                break;
            }
            case R.id.action_map: {
                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container2, new MapFragment(),"user_map")
                        .hide(getActivity().getSupportFragmentManager().findFragmentByTag("1"))
                        .addToBackStack("MAP_FRAGMENT")
                        .commit();
                break;
            }
            case R.id.action_search: {

            }
        }
        return super.onOptionsItemSelected(item);
    }
}
