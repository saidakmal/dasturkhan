package uz.N00003988.dasturkhan.ui.main.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Busy;
import uz.N00003988.dasturkhan.model.Data;
import uz.N00003988.dasturkhan.model.Model;
import uz.N00003988.dasturkhan.model.Rating;
import uz.N00003988.dasturkhan.model.TableModel;
import uz.N00003988.dasturkhan.ui.main.adapters.ManagerDataRecyclerViewAdapter;
import uz.N00003988.dasturkhan.ui.main.holders.ManagerDataViewHolder;
import uz.N00003988.dasturkhan.ui.manager.ItemManagerFragment;
import uz.N00003988.dasturkhan.ui.manager.add.AddItemFragment;
import uz.N00003988.dasturkhan.ui.manager.notification.NotificationFragment;
import uz.N00003988.dasturkhan.ui.manager.table.ItemTableFragment;
import uz.N00003988.dasturkhan.ui.user.fragments.UserCategoriesItemFragment;

public class ManagerFragment extends Fragment implements ManagerDataViewHolder.OnItemClickListener {
    private ArrayList<Data> data;
    private ArrayList<String> keys;
    private FirebaseFirestore firebaseFirestore;
    private RecyclerView recyclerView;
    private FloatingActionButton fab;
    private Toolbar toolbar;


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
            refresh();
        }
    }

    public ManagerFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        data = new ArrayList<>();
        keys = new ArrayList<>();
        firebaseFirestore = FirebaseFirestore.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manager, container, false);
        recyclerView = view.findViewById(R.id.manager_data_list);
        fab = view.findViewById(R.id.fab);
        toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        fab.setOnClickListener(v -> {
            getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container2, AddItemFragment.instance(false), "add_item")
                    .hide(getActivity().getSupportFragmentManager().findFragmentByTag("2"))
                    .addToBackStack("ADD_ITEM")
                    .commit();
//            add();
//            edit();
//            delete();
        });
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refresh();
    }


    private void refresh() {
        firebaseFirestore
                .collection("dasturkhan")
                .whereEqualTo("uid", FirebaseAuth.getInstance().getCurrentUser().getUid())
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        data = new ArrayList<>();
                        keys = new ArrayList<>();
                        QuerySnapshot queryDocumentSnapshots = task.getResult();
                        for (int i = 0; i < queryDocumentSnapshots.size(); i++) {
                            data.add(queryDocumentSnapshots.getDocuments().get(i).toObject(Data.class));
                            keys.add(queryDocumentSnapshots.getDocuments().get(i).getId());
                        }
                        recyclerView.setAdapter(new ManagerDataRecyclerViewAdapter(data, this));
                    }

                });
    }

    @Override
    public void onItemClick(int pos) {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container2, ItemManagerFragment.instance(keys.get(pos), data.get(pos), true), "item")
                .hide(getActivity().getSupportFragmentManager().findFragmentByTag("2"))
                .addToBackStack("ITEM_MANAGER")
                .commit();

//        Toast.makeText(getContext(), keys.get(pos) + " " + data.get(pos).getModel().getName(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onAddImageClick(int pos) {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container2, ItemTableFragment.instance(keys.get(pos), data.get(pos)))
                .hide(getActivity().getSupportFragmentManager().findFragmentByTag("2"))
                .addToBackStack("ITEM_TABLE")
                .commit();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_manager, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_mes_manager) {
            getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container2, new NotificationFragment())
                    .hide(getActivity().getSupportFragmentManager().findFragmentByTag("2"))
                    .addToBackStack("NOTIFICATION_FRAGMENT")
                    .commit();
        }
        return super.onOptionsItemSelected(item);
    }
}

