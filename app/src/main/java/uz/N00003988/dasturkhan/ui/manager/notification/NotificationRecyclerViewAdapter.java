package uz.N00003988.dasturkhan.ui.manager.notification;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;

public class NotificationRecyclerViewAdapter extends RecyclerView.Adapter<NotificationViewHolder> {
    private ArrayList<NotificationModel> notificationModels;
    private NotificationViewHolder.OnAnsweredLisnener onAnsweredLisnener;

    public NotificationRecyclerViewAdapter(ArrayList<NotificationModel> notificationModels, NotificationViewHolder.OnAnsweredLisnener onAnsweredLisnener) {
        this.notificationModels = notificationModels;
        this.onAnsweredLisnener = onAnsweredLisnener;
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new NotificationViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_notification_manager_layout, viewGroup, false), onAnsweredLisnener);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder notificationViewHolder, int i) {
        notificationViewHolder.bindData(notificationModels.get(i));
    }

    @Override
    public int getItemCount() {
        return notificationModels.size();
    }
}
