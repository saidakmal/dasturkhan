package uz.N00003988.dasturkhan.ui.manager.comments;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Rating;

public class CommentsRecyclerViewHolder extends RecyclerView.ViewHolder {
    private AppCompatRatingBar appCompatRatingBar;
    private TextView name;
    private TextView comment;


    public CommentsRecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        appCompatRatingBar = itemView.findViewById(R.id.rating_comment);
        name = itemView.findViewById(R.id.name_comment);
        comment = itemView.findViewById(R.id.text_comment);
    }

    @SuppressLint("SetTextI18n")
    public void binData(Rating rating) {
        appCompatRatingBar.setRating(rating.getMark());
        name.setText(rating.getRatingName() + "");
        comment.setText(rating.getRatingText() + " ");
    }
}
