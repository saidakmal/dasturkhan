package uz.N00003988.dasturkhan.ui.user.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Data;
import uz.N00003988.dasturkhan.room.database.Database;
import uz.N00003988.dasturkhan.ui.manager.ItemManagerFragment;
import uz.N00003988.dasturkhan.ui.user.busy.UserBusyFragment;
import uz.N00003988.dasturkhan.ui.user.holders.UserDataViewHolder;
import uz.N00003988.dasturkhan.ui.user.adapters.UserDataRecyclerViewAdapter;


public class UserAllFragment extends Fragment implements UserDataViewHolder.OnItemClickListener {
    private FirebaseFirestore firebaseFirestore;
    private ArrayList<Data> data;
    private ArrayList<String> keys;
    private RecyclerView recyclerView;
    private static final String TAG = "asgahdjr";
    private int isFavourite = 0;

    public static UserAllFragment instane(int isFavourite) {
        UserAllFragment userAllFragment = new UserAllFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("FAVOURITE", isFavourite);  // 1 true
        userAllFragment.setArguments(bundle);
        return userAllFragment;
    }

    public UserAllFragment() {
        data = new ArrayList<>();
        keys = new ArrayList<>();
        firebaseFirestore = FirebaseFirestore.getInstance();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "atttach");
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        Log.d(TAG, "child atttach");

    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "dettach");

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "on start");

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "on resume");

    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.d(TAG, "hiddin " + hidden);
        if (!hidden)
            refresh();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            refresh();
        }
        Log.d(TAG, "visible to user " + isVisibleToUser);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            isFavourite = getArguments().getInt("FAVOURITE");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_all, container, false);
        recyclerView = view.findViewById(R.id.user_all_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refresh();
    }

    private void refresh() {
        firebaseFirestore
                .collection("dasturkhan")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        data = new ArrayList<>();
                        keys = new ArrayList<>();
                        QuerySnapshot queryDocumentSnapshots = task.getResult();
                        for (int i = 0; i < queryDocumentSnapshots.size(); i++) {
                            if (isFavourite == 0) {
                                keys.add(queryDocumentSnapshots.getDocuments().get(i).getId());
                                data.add(queryDocumentSnapshots.getDocuments().get(i).toObject(Data.class));
                            } else {
                                if (Database.getDatabase().getFavoriteDao().isFavourite(queryDocumentSnapshots.getDocuments().get(i).getId()) != null) {
                                    keys.add(queryDocumentSnapshots.getDocuments().get(i).getId());
                                    data.add(queryDocumentSnapshots.getDocuments().get(i).toObject(Data.class));
                                }
                            }
                        }
//                        updateMarkUser();
                        recyclerView.setAdapter(new UserDataRecyclerViewAdapter(keys, data, this));
                    }
                });
    }

//    private void updateMarkUser() {
//
//    }

    @Override
    public void onItemClick(int pos) {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container2, ItemManagerFragment.instance(keys.get(pos), data.get(pos), false), "item")
                .hide(getActivity().getSupportFragmentManager().findFragmentByTag("1"))
                .addToBackStack("ITEM_USER")
                .commit();

//        Toast.makeText(getContext(), "" + data.get(pos).getModel().getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddImageClick(int pos) {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container2, UserBusyFragment.instance(keys.get(pos), data.get(pos)))
                .hide(getActivity().getSupportFragmentManager().findFragmentByTag("1"))
                .addToBackStack("USER_BUSY")
                .commit();
    }


}
