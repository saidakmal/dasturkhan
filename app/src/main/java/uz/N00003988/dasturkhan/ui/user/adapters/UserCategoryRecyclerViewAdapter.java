package uz.N00003988.dasturkhan.ui.user.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.ui.user.holders.UserCategoryViewHolder;
import uz.N00003988.dasturkhan.ui.user.model.CategoryData;

public class UserCategoryRecyclerViewAdapter extends RecyclerView.Adapter<UserCategoryViewHolder> {
    private ArrayList<CategoryData> categoryData;
    private UserCategoryViewHolder.OnItemClickListener onItemClickListener;

    public UserCategoryRecyclerViewAdapter(ArrayList<CategoryData> categoryData, UserCategoryViewHolder.OnItemClickListener onItemClickListener) {
        this.categoryData = categoryData;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public UserCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new UserCategoryViewHolder(
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user_category, viewGroup, false),
                onItemClickListener
        );
    }

    @Override
    public void onBindViewHolder(@NonNull UserCategoryViewHolder userCategoryViewHolder, int i) {
        userCategoryViewHolder.bind(categoryData.get(i));

    }

    @Override
    public int getItemCount() {
        return categoryData.size();
    }
}
