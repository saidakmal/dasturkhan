package uz.N00003988.dasturkhan.ui.main.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import uz.N00003988.dasturkhan.ui.user.fragments.UserAllFragment;
import uz.N00003988.dasturkhan.ui.user.fragments.UserCategoryFragment;
import uz.N00003988.dasturkhan.ui.user.fragments.UserFavouriteFragment;

public class UserViewPagerAdapter extends FragmentPagerAdapter {

    public UserViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        if (i == 0) {
            UserAllFragment userAllFragment = new UserAllFragment();
            return userAllFragment;
        } else if (i == 1)
            return new UserCategoryFragment();
        else {
            return UserAllFragment.instane(1);
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String s = " ";
        switch (position) {
            case 0:
                s = "ALL LIST";
                break;
            case 1:
                s = "CATEGORY";
                break;
            case 2:
                s = "FAVOURITES";
                break;
        }
        return s;
    }


}
