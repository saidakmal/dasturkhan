package uz.N00003988.dasturkhan.ui.manager;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Data;
import uz.N00003988.dasturkhan.model.Rating;
import uz.N00003988.dasturkhan.ui.manager.add.AddItemFragment;
import uz.N00003988.dasturkhan.ui.manager.comments.ViewAllFragment;
import uz.N00003988.dasturkhan.ui.manager.pager.ItemManagerViewPagerAdapter;

public class ItemManagerFragment extends Fragment {
    public static final String KEY = "KEY";
    public static final String DATA = "DATA";
    public static final String MANAGER = "MANAGER";
    public String uid;
    private String key;
    private Data data;
    private Toolbar toolbar;
    private TextView ratingText;
    private AppCompatRatingBar ratingView;
    public ViewPager viewPager;
    private boolean isManager;
    private FloatingActionButton fab;
    private ViewGroup viewGroup;
    int mark = -1;
    String ratingName = null;
    String ratingTextComment = null;
    private TextView name;
    private TextView category;
    private TextView number;
    private TextView address;

    private TextView textRatingAll;
    private TextView viewAll;
    private AppCompatRatingBar appCompatRatingBarAll;


    public ItemManagerFragment() {

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
//        if (!hidden) {
        if (key != null) {
            FirebaseFirestore.getInstance().collection("dasturkhan").document(key).get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Toast.makeText(getContext(), "refresh resreshs", Toast.LENGTH_SHORT).show();
                            data = task.getResult().toObject(Data.class);
                            viewPager.setAdapter(new ItemManagerViewPagerAdapter(getChildFragmentManager(), data.getModel().getPhotos()));
                            viewPager.getAdapter().notifyDataSetChanged();
                            refresh();
                        }
                    });
        }
    }
//    }

    public static ItemManagerFragment instance(String key, Data data, boolean isManager) {
        ItemManagerFragment itemManagerFragment = new ItemManagerFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY, key);
        bundle.putSerializable(DATA, data);
        bundle.putSerializable(MANAGER, isManager);
        itemManagerFragment.setArguments(bundle);
        return itemManagerFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        if (getArguments() != null) {
            key = (String) getArguments().getSerializable(KEY);
            data = (Data) getArguments().getSerializable(DATA);
            isManager = (boolean) getArguments().getSerializable(MANAGER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_manager, container, false);
        fab = view.findViewById(R.id.fab);
        toolbar = view.findViewById(R.id.toolbar);
        ratingText = view.findViewById(R.id.rating_text_item_manager);
        ratingView = view.findViewById(R.id.rating);
        viewPager = view.findViewById(R.id.pager_item_manager);
        viewGroup = view.findViewById(R.id.all_rating_view_group);
        name = view.findViewById(R.id.name_item_data);
        category = view.findViewById(R.id.category_item_data);
        number = view.findViewById(R.id.manager_number_item_data);
        address = view.findViewById(R.id.address_item_data);
        viewAll = view.findViewById(R.id.view_all);
        textRatingAll = view.findViewById(R.id.all_rating_text);
        appCompatRatingBarAll = view.findViewById(R.id.rating_all);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refresh();

    }

    private void refresh() {
        int size = data.getModel().getRating().size();
        float rating = 0;
        int sum = 0;
        for (int i = 0; i < size; i++) {
            sum += data.getModel().getRating().get(i).getMark();
        }
        if (size != 0) {
            rating = (float) sum / (float) size;
        }
        appCompatRatingBarAll.setRating(rating);
        String s;
        if (size > 0) {
            if (size > 1) s = rating + "/" + size + " ratings";
            else s = rating + "/" + "1 rating";
        } else s = "No ratings";
        textRatingAll.setText(s);
        if (isManager) {
            ratingView.setRating(rating);
            ratingText.setText(s);
        } else {

            for (int i = 0; i < data.getModel().getRating().size(); i++) {
                if (uid.equals(data.getModel().getRating().get(i).getUid())) {
                    mark = data.getModel().getRating().get(i).getMark();
                    break;
                }
            }
            if (mark != -1) {
                updateMarkView();
            } else {
                ratingText.setText("Please rate");
//                viewGroup.setVisibility(View.GONE);
            }
        }
        viewPager.setAdapter(new ItemManagerViewPagerAdapter(getChildFragmentManager(), data.getModel().getPhotos()));
        viewPager.getAdapter().notifyDataSetChanged();

        if (isManager) {
            fab.setOnClickListener(this::onManagerFabClick);
        } else {
            fab.setOnClickListener(this::onUserFabClick);
            fab.setImageResource(R.drawable.ic_star_black_24dp);
        }

        name.setText(data.getModel().getName());
        category.setText(data.getCategory());
        number.setText(data.getModel().getNumber());
        address.setText(data.getModel().getAddress());

        viewAll.setOnClickListener(v -> {
            getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
//                    .add(R.id.container2, new ViewAllFragment())
                    .add(R.id.container2, ViewAllFragment.instance(data))
                    .hide(getActivity().getSupportFragmentManager().findFragmentByTag("item"))
                    .addToBackStack("VIEW_ALL")
                    .commit();
            Toast.makeText(getContext(), "view all", Toast.LENGTH_SHORT).show();
        });
    }

    private void onManagerFabClick(View view) {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container2, AddItemFragment.instance(key, data, true), "add_item")
                .addToBackStack("ADD_ITEM")
                .commit();

        getActivity().getSupportFragmentManager().beginTransaction()
                .remove(getActivity().getSupportFragmentManager().findFragmentByTag("item"))
                .commit();
    }

    private void onUserFabClick(View view) {
        View v = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_rating_layout, null, false);
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle("Your Rating")
                .setPositiveButton("OK", (dialog, which) -> {
                    if (mark != -1) {
                        showNextDialog(mark);
                    }
                    dialog.cancel();
                })
                .setNegativeButton("Cancel", (dialog, which) -> dialog.cancel())
                .show();
        RadioGroup radioGroup = alertDialog.findViewById(R.id.radio_group);
        if (radioGroup != null) {
            radioGroup.clearCheck();
            radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                RadioButton radioButton = group.findViewById(checkedId);
                if (radioButton != null && checkedId != -1) {
                    mark = 5 - radioGroup.indexOfChild(radioButton);
                }
            });
        }

    }

    private void showNextDialog(int mark) {
        View v = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_rating_comment_layout, null, false);
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle("Comment")
                .setPositiveButton("COMMENT", (dialog, which) -> {
                    if (ratingName != null && ratingTextComment != null) {
                        markData(mark, ratingName, ratingTextComment);
                    } else {
                        Toast.makeText(getContext(), "Fail", Toast.LENGTH_SHORT).show();
                    }
                    dialog.cancel();
                })
                .setNegativeButton("Cancel", (d, w) -> {
                    d.cancel();
                }).show();

        EditText name = alertDialog.findViewById(R.id.rating_name);
        EditText comment = alertDialog.findViewById(R.id.rating_comment);
        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ratingName = s.toString();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ratingTextComment = s.toString();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void markData(int mark, String ratingName, String ratingText) {
        boolean marked = false;
        Rating rating = new Rating(uid, mark, ratingName, ratingText);
        ArrayList<Rating> ratings = data.getModel().getRating();
        for (int i = 0; i < ratings.size(); i++) {
            if (ratings.get(i).getUid().equals(uid)) {
                ratings.set(i, rating);
                marked = true;
            }
        }

        if (!marked) {
            ratings.add(rating);
        }

        data.getModel().setRating(ratings);
        FirebaseFirestore.getInstance().collection("dasturkhan").document(key).set(data)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        updateMarkView();
                        Toast.makeText(getContext(), "Thanks for rating!", Toast.LENGTH_SHORT).show();
                    }

                });
    }

    private void updateMarkView() {
        viewGroup.setVisibility(View.VISIBLE);
        ratingView.setRating(mark);
        ratingText.setText("Your Rating");
        int size = data.getModel().getRating().size();
        float rating = 0;
        int sum = 0;
        for (int i = 0; i < size; i++) {
            sum += data.getModel().getRating().get(i).getMark();
        }
        if (size != 0) {
            rating = (float) sum / (float) size;
        }
        appCompatRatingBarAll.setRating(rating);
        String s;
        if (size > 0) {
            if (size > 1) s = rating + "/" + size + " ratings";
            else s = rating + "/" + "1 rating";
        } else s = "No ratings";
        textRatingAll.setText(s);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (isManager) {
            getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .show(getActivity()
                            .getSupportFragmentManager()
                            .findFragmentByTag("2"))
                    .commit();
        }
    }
}
