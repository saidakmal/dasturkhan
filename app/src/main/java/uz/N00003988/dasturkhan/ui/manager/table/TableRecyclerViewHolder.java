package uz.N00003988.dasturkhan.ui.manager.table;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.TableModel;

public class TableRecyclerViewHolder extends RecyclerView.ViewHolder {
    private TextView numberTable;
    private TextView name;
    private TextView number;
    private TextView notBusy;
    private ImageView more;
    private OnItemClickListener onItemClickListener;
    private Context context;


    public TableRecyclerViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener) {
        super(itemView);
        context = itemView.getContext();
        this.onItemClickListener = onItemClickListener;
        numberTable = itemView.findViewById(R.id.text_number);
        name = itemView.findViewById(R.id.text_name_table);
        number = itemView.findViewById(R.id.text_number_table);
        notBusy = itemView.findViewById(R.id.text_busy_table);
        more = itemView.findViewById(R.id.item_table_more);
        more.setOnClickListener(v -> {
            if (onItemClickListener != null)
                onItemClickListener.onMoreItemClick(getAdapterPosition(), more);
        });
    }

    @SuppressLint("SetTextI18n")
    public void bindData(boolean isManager, int num, boolean isBusy, TableModel tableModel) {
        numberTable.setText(num + "");
        if (isBusy) {
            if (isManager) {
                name.setText(tableModel.getName() + " at " + tableModel.getTime());
                number.setText(tableModel.getNumber());
            } else {
                notBusy.setText("Busy");
                notBusy.setTextColor(context.getResources().getColor(R.color.colorAccent));
                more.setVisibility(View.INVISIBLE);
            }
        } else {
            more.setVisibility(View.VISIBLE);
            // table model null
            notBusy.setText("Not busy");
        }
    }

    public interface OnItemClickListener {
        void onMoreItemClick(int pos, View view);
    }


}
