package uz.N00003988.dasturkhan.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.ui.main.MainActivity;

public class SmsFragment extends Fragment {
    private static final String NUMBER = "NUMBER";
    private TextView textViewNumber;
    private EditText editTextSms;
    private Button ok;
    private String number;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    private String verId;


    public SmsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        number = (String) getArguments().get(NUMBER);
    }


    public static SmsFragment instance(String number) {
        SmsFragment smsFragment = new SmsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(NUMBER, number);
        smsFragment.setArguments(bundle);
        return smsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sms, container, false);
        textViewNumber = view.findViewById(R.id.tv_number);
        editTextSms = view.findViewById(R.id.et_sms);
        ok = view.findViewById(R.id.bt_ok);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textViewNumber.setText(number);
        ok.setOnClickListener(v -> {
            String sms = editTextSms.getText().toString();
            signIn(PhoneAuthProvider.getCredential(verId, sms));
        });

        createCallBacks();


    }


    private void createCallBacks() {
        callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                Toast.makeText(getActivity(), "onVerificationCompleted " + phoneAuthCredential.getSmsCode(), Toast.LENGTH_SHORT).show();
                if (phoneAuthCredential.getSmsCode() != null) {
                    editTextSms.setText(phoneAuthCredential.getSmsCode());
                }
                signIn(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(getActivity(), "onVerificationFailed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                verId = s;
            }

            @Override
            public void onCodeAutoRetrievalTimeOut(String s) {
                super.onCodeAutoRetrievalTimeOut(s);
                Toast.makeText(getActivity(), "onCodeAutoRetrievalTimeOut", Toast.LENGTH_SHORT).show();
            }
        };
        verifyPhoneNumber();
    }

    private void verifyPhoneNumber() {
        if (getActivity() != null) {
            PhoneAuthProvider
                    .getInstance()
                    .verifyPhoneNumber(
                            number,
                            70,
                            TimeUnit.SECONDS,
                            (AppCompatActivity) getActivity(),
                            (PhoneAuthProvider.OnVerificationStateChangedCallbacks) callbacks
                    );
        } else {
            Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
        }

    }

    public void signIn(PhoneAuthCredential credential) {
        FirebaseAuth.getInstance()
                .signInWithCredential(credential)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Toast.makeText(getActivity(), "Sign in successful", Toast.LENGTH_SHORT).show();
                        updateUI();
                    } else {
                        Toast.makeText(getActivity(), "Failed Sign in", Toast.LENGTH_SHORT).show();

                    }
                });

    }

    private void updateUI() {
        startActivity(new Intent(getActivity(), MainActivity.class));
        getActivity().finish();
    }

}
