package uz.N00003988.dasturkhan.ui.user.busy;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.JsonParser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Busy;
import uz.N00003988.dasturkhan.model.Data;
import uz.N00003988.dasturkhan.model.TableModel;
import uz.N00003988.dasturkhan.retrofit.ApiInterface;
import uz.N00003988.dasturkhan.retrofit.ApiRetrofit;
import uz.N00003988.dasturkhan.retrofit.models.MessageData;
import uz.N00003988.dasturkhan.retrofit.models.ResponseData;
import uz.N00003988.dasturkhan.ui.manager.notification.NotificationModel;
import uz.N00003988.dasturkhan.ui.manager.table.TableRecyclerViewAdapter;
import uz.N00003988.dasturkhan.ui.manager.table.TableRecyclerViewHolder;


public class UserBusyFragment extends Fragment implements TableRecyclerViewHolder.OnItemClickListener {
    public static final String KEY = "KEY";
    public static final String DATA = "DATA";
    private String key;
    private Data data;
    private Busy busy = null;
    private Calendar calendar = Calendar.getInstance();
    int year = -1;
    int month = -1;
    int dayOfMonth = -1;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private Toolbar toolbar;
    private TextView sub;
    private RecyclerView recyclerView;
    public String nameText;
    public String numberText;
    public String timeText;

    public UserBusyFragment() {
    }

    public static UserBusyFragment instance(String key, Data data) {
        UserBusyFragment userBusyFragment = new UserBusyFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(DATA, data);
        bundle.putString(KEY, key);
        userBusyFragment.setArguments(bundle);
        return userBusyFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            key = getArguments().getString(KEY);
            data = getArguments().getParcelable(DATA);
            String date = simpleDateFormat.format(calendar.getTimeInMillis());
            for (int i = 0; i < data.getBusy().size(); i++) {
                if (date.equals(data.getBusy().get(i).getDate())) {
                    busy = data.getBusy().get(i);
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_busy, container, false);
        toolbar = view.findViewById(R.id.toolbar);
        sub = view.findViewById(R.id.sub_text_toolbar);
        recyclerView = view.findViewById(R.id.recycler_view_table);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sub.setText(simpleDateFormat.format(calendar.getTimeInMillis()));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new TableRecyclerViewAdapter(false, data, busy, this));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.user_table_fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        } else if (item.getItemId() == R.id.calendar) {
            View v = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_calendar_layout, null, false);
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                    .setView(v)
                    .setPositiveButton("OK", (dialog, which) -> {
                        if (year != -1 && month != -1 && dayOfMonth != -1) {
                            calendar = new GregorianCalendar(year, month, dayOfMonth);
                            sub.setText(simpleDateFormat.format(calendar.getTimeInMillis()));
                            String date = simpleDateFormat.format(calendar.getTimeInMillis());
                            for (int i = 0; i < data.getBusy().size(); i++) {
                                if (date.equals(data.getBusy().get(i).getDate())) {
                                    busy = data.getBusy().get(i);
                                }
                            }
                            recyclerView.setAdapter(new TableRecyclerViewAdapter(false, data, busy, this));
                        }
                        dialog.cancel();
                    })
                    .setNegativeButton("Cancel", (dialog, which) -> dialog.cancel())
                    .show();

            CalendarView calendarView = alertDialog.findViewById(R.id.calendar_dialog);
            if (calendarView != null) {
                calendarView.setDate(calendar.getTimeInMillis(), false, false);
                calendarView.setOnDateChangeListener((view1, year, month, dayOfMonth) -> {
                    busy = null;
                    this.year = year;
                    this.month = month;
                    this.dayOfMonth = dayOfMonth;
                });
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMoreItemClick(int pos, View view) {
        PopupMenu popupMenu = new PopupMenu(getContext(), view);
        popupMenu.inflate(R.menu.menu_set_busy_table);
        popupMenu.setOnMenuItemClickListener(menuItem -> {
            int id = menuItem.getItemId();
            if (id == R.id.action_set_busy) {
                View v = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_edit_table_layout, null, false);
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                        .setView(v)
                        .setTitle("Send notification to manager")
                        .setPositiveButton("SEND", (dialog, which) -> {
                            String topic = data.getUid();
                            String whoFor = "manager";
                            String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                            String name = nameText;
                            String number = numberText;
                            int tableNumber = pos + 1;
                            String time = timeText;
                            String date = simpleDateFormat.format(calendar.getTimeInMillis());
                            String managerId = data.getUid();
                            String idItem = key;
                            String nameItem = data.getModel().getName();
                            String managerAnswered = "no";
                            String accepted = "no";
                            NotificationModel notificationModel = new NotificationModel(
                                   whoFor, userId, name, number, tableNumber, time, date, managerId, idItem, nameItem, managerAnswered, accepted);

                            MessageData messageData = new MessageData(topic,  notificationModel);
                            Log.d("QAWSED", messageData.toString());

                            ApiInterface apiInterface = ApiRetrofit.getRetrofit().create(ApiInterface.class);
                            Call<ResponseData> call = apiInterface.sendMessage(new JsonParser().parse(messageData.toString()).getAsJsonObject());
                            call.enqueue(new Callback<ResponseData>() {
                                @Override
                                public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                                    if (response.isSuccessful()) {
                                        FirebaseFirestore.getInstance()
                                                .collection("notification")
                                                .add(notificationModel);
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseData> call, Throwable t) {
                                    Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
                                }
                            });


                            dialog.cancel();
                        })
                        .setNegativeButton("Cancel", (dialog, which) -> dialog.cancel())
                        .show();
                EditText name = alertDialog.findViewById(R.id.name_table_busy);
                EditText number = alertDialog.findViewById(R.id.number_table_busy);
                EditText time = alertDialog.findViewById(R.id.time_table_busy);
                if (name != null && number != null && time != null) {
                    nameText = name.getText().toString();
                    numberText = number.getText().toString();
                    timeText = time.getText().toString();
                    name.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            nameText = s.toString();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });
                    number.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            numberText = s.toString();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });
                    time.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            timeText = s.toString();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });
                }
            }
            return true;
        });
        popupMenu.show();
    }
}
