package uz.N00003988.dasturkhan.ui.manager.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class ItemManagerViewPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<String> urls;

    public ItemManagerViewPagerAdapter(FragmentManager fm, ArrayList<String> urls) {
        super(fm);
        this.urls = urls;
    }

    @Override
    public Fragment getItem(int i) {
        return ItemImageFragment.instance(urls.get(i), i+1, urls.size());
    }

    @Override
    public int getCount() {
        return urls.size();
    }
}
