package uz.N00003988.dasturkhan.ui.manager.add;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;

public class AddPhotoRecyclerViewAdapter extends RecyclerView.Adapter<AddPhotosViewHolder> {
    private ArrayList<String> photos;
    private AddPhotosViewHolder.OnRemoveListener onRemoveListener;

    public AddPhotoRecyclerViewAdapter(ArrayList<String> photos, AddPhotosViewHolder.OnRemoveListener onRemoveListener) {
        this.photos = photos;
        this.onRemoveListener = onRemoveListener;
    }

    @NonNull
    @Override
    public AddPhotosViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AddPhotosViewHolder(
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_add_photo, viewGroup, false),
                onRemoveListener
        );
    }

    @Override
    public void onBindViewHolder(@NonNull AddPhotosViewHolder addPhotosViewHolder, int i) {
        addPhotosViewHolder.bindData(photos.get(i));
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }
}
