package uz.N00003988.dasturkhan.ui.manager.notification;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;

public class NotificationViewHolder extends RecyclerView.ViewHolder {
    private OnAnsweredLisnener onAnsweredLisnener;
    private TextView nameItem;
    private TextView name;
    private TextView number;
    private TextView dataAndTime;
    private TextView cancel;
    private TextView accept;

    public NotificationViewHolder(@NonNull View itemView, OnAnsweredLisnener onAnsweredLisnener) {
        super(itemView);
        this.onAnsweredLisnener = onAnsweredLisnener;
        nameItem = itemView.findViewById(R.id.notif_item_name);
        name = itemView.findViewById(R.id.notif_name_user);
        number = itemView.findViewById(R.id.notif_number_user);
        dataAndTime = itemView.findViewById(R.id.notif_date_and_time);
        cancel = itemView.findViewById(R.id.notif_cancel);
        accept = itemView.findViewById(R.id.notif_accept);
        cancel.setOnClickListener(this::cancel);
        accept.setOnClickListener(this::accept);

    }

    @SuppressLint("SetTextI18n")
    public void bindData(NotificationModel model) {
        nameItem.setText(model.getNameItem());
        name.setText(model.getName());
        number.setText(model.getNumber());
        dataAndTime.setText(model.getDate() + " " + model.getTime() + "  (" + model.getTableNumber() + "-table)");
    }

    private void cancel(View view) {
        onAnsweredLisnener.onCancel(getAdapterPosition());

    }

    private void accept(View view) {
        onAnsweredLisnener.onAccept(getAdapterPosition());
    }

    public interface OnAnsweredLisnener {
        void onCancel(int pos);

        void onAccept(int pos);
    }
}
