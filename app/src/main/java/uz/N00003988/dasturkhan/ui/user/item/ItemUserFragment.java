package uz.N00003988.dasturkhan.ui.user.item;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uz.N00003988.dasturkhan.R;

public class ItemUserFragment extends Fragment {

    // item manager fragment
    public ItemUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_item_user, container, false);
    }

}
