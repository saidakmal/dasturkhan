package uz.N00003988.dasturkhan.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Rating;


public class RatingView extends LinearLayout {
    private ImageView[] stars = new ImageView[5];

    public RatingView(Context context) {
        super(context);
        init(context, null);
    }

    public RatingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RatingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RatingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.custom_rating_view, this, true);
        stars[0] = findViewById(R.id.star1);
        stars[1] = findViewById(R.id.star2);
        stars[2] = findViewById(R.id.star3);
        stars[3] = findViewById(R.id.star4);
        stars[4] = findViewById(R.id.star5);

    }

    public void setRating(ArrayList<Rating> ratings) {
        int rating;
        int sum = 0;
        int size = ratings.size();
        for (int i = 0; i < size; i++) {
            sum += ratings.get(i).getMark();
        }
        rating = sum / size;

        for (int i = 0; i < stars.length; i++) {
            if (rating >= i + 1) stars[i].setImageResource(R.drawable.ic_star_black_24dp);
            else stars[i].setImageResource(R.drawable.ic_star_border_black_24dp);
        }
    }
}
