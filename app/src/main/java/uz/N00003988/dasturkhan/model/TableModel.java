package uz.N00003988.dasturkhan.model;

import java.io.Serializable;

public class TableModel implements Serializable {
    private String name;
    private String number;
    private int tableNumber;
    private String time;

    public TableModel() {
    }

    public TableModel(String name, String number, int tableNumber, String time) {
        this.name = name;
        this.number = number;
        this.tableNumber = tableNumber;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
