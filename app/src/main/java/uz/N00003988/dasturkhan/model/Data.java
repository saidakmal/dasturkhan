package uz.N00003988.dasturkhan.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.GeoPoint;

import java.io.Serializable;
import java.util.ArrayList;

public class Data implements Serializable, Parcelable {
    private GeoPoint location;
    private ArrayList<Busy> busy;
    private Model model;
    private String category;
    private String uid;

    public Data() {
    }

    public Data(GeoPoint location, ArrayList<Busy> busy, Model model, String category, String uid) {
        this.location = location;
        this.busy = busy;
        this.model = model;
        this.category = category;
        this.uid = uid;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ArrayList<Busy> getBusy() {
        return busy;
    }

    public void setBusy(ArrayList<Busy> busy) {
        this.busy = busy;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
