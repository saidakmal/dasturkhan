package uz.N00003988.dasturkhan.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Model implements Serializable {
    private String name;
    private String number;
    private String address;
    private ArrayList<Rating> rating;
    private ArrayList<Integer> tableNumbers;
    private ArrayList<String> photos;

    public Model() {
    }

    public Model(String name, String number, String address, ArrayList<Rating> rating, ArrayList<Integer> tableNumbers, ArrayList<String> photos) {
        this.name = name;
        this.number = number;
        this.address = address;
        this.rating = rating;
        this.tableNumbers = tableNumbers;
        this.photos = photos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<Rating> getRating() {
        return rating;
    }

    public void setRating(ArrayList<Rating> rating) {
        this.rating = rating;
    }

    public ArrayList<Integer> getTableNumbers() {
        return tableNumbers;
    }

    public void setTableNumbers(ArrayList<Integer> tableNumbers) {
        this.tableNumbers = tableNumbers;
    }

    public ArrayList<String> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<String> photos) {
        this.photos = photos;
    }
}
