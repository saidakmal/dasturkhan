package uz.N00003988.dasturkhan.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Busy implements Serializable {
    private String date;
    private ArrayList<TableModel> tables;

    public Busy() {
    }

    public Busy(String date, ArrayList<TableModel> tables) {
        this.date = date;
        this.tables = tables;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<TableModel> getTables() {
        return tables;
    }

    public void setTables(ArrayList<TableModel> tables) {
        this.tables = tables;
    }
}
