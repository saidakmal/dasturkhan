package uz.N00003988.dasturkhan.model;

public class SendBoolen {
private boolean downloadEnd;

    public SendBoolen(boolean downloadEnd) {
        this.downloadEnd = downloadEnd;
    }

    public boolean isDownloadEnd() {
        return downloadEnd;
    }
}
