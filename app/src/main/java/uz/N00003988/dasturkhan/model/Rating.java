package uz.N00003988.dasturkhan.model;

import java.io.Serializable;

public class Rating implements Serializable {
    private String uid;
    private int mark;
    private String ratingName;
    private String ratingText;

    public Rating() {
    }

    public Rating(String uid, int mark, String ratingName, String ratingText) {
        this.uid = uid;
        this.mark = mark;
        this.ratingName = ratingName;
        this.ratingText = ratingText;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public String getRatingName() {
        return ratingName;
    }

    public void setRatingName(String ratingName) {
        this.ratingName = ratingName;
    }

    public String getRatingText() {
        return ratingText;
    }

    public void setRatingText(String ratingText) {
        this.ratingText = ratingText;
    }
}
