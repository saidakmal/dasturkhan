package uz.N00003988.dasturkhan.app;

import android.support.multidex.MultiDexApplication;

import com.google.firebase.FirebaseApp;

import uz.N00003988.dasturkhan.room.database.Database;
import uz.N00003988.dasturkhan.settings.Settings;

public class App extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        Settings.init(getApplicationContext());
        FirebaseApp.initializeApp(this);
        Database.init(getApplicationContext());
    }

}
