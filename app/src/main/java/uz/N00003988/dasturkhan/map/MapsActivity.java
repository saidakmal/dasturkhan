package uz.N00003988.dasturkhan.map;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.firestore.GeoPoint;

import java.util.ArrayList;
import java.util.List;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.room.database.Database;
import uz.N00003988.dasturkhan.room.model.FavoriteModel;

public class MapsActivity extends AppCompatActivity {

    private Button button;
    private int MY_PERMISSIONS_REQUEST_LOCATION = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                MY_PERMISSIONS_REQUEST_LOCATION);
        button = findViewById(R.id.btn_test);
        ArrayList<MapModel> mapModels= new ArrayList<>();
        mapModels.add(new MapModel("sdfa",new LatLng(41.541468,69.584219)));
        mapModels.add(new MapModel("sdfa",new LatLng(41.841468,69.784219)));
        mapModels.add(new MapModel("sdfa",new LatLng(41.941468,69.184219)));
        Toast.makeText(this, "" + Database.getDatabase().getFavoriteDao().getFavorites().size(), Toast.LENGTH_SHORT).show();

        button.setOnClickListener(v -> {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.con, MapFragment.newInstance(mapModels),"user_map");
            transaction.addToBackStack(null);
            transaction.commit();
//            FavoriteModel favoriteModel = new FavoriteModel("dsfadasd", 1);
//            Database.getDatabase().getFavoriteDao().insertFavoriteData(favoriteModel);
//            Toast.makeText(this, "" + Database.getDatabase().getFavoriteDao().getFavorites().size(), Toast.LENGTH_SHORT).show();
        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
}
