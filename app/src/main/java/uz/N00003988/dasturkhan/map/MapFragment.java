package uz.N00003988.dasturkhan.map;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.model.Data;
import uz.N00003988.dasturkhan.ui.user.adapters.UserDataRecyclerViewAdapter;
import uz.N00003988.dasturkhan.ui.user.busy.UserBusyFragment;


public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private GoogleMap mMap;
    private MapView mapView;

    private Toolbar toolbar;
    //    private ArrayList<MapModel> data;
    private ArrayList<Marker> markers;
    private AsyncTask asyncTask;

    private FirebaseFirestore firebaseFirestore;
    private ArrayList<Data> listData;
    private ArrayList<String> keys;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();

    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maps, container, false);
        mapView = view.findViewById(R.id.map);
        toolbar = view.findViewById(R.id.toolbar);
//        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        data = getArguments().getParcelableArrayList("data");
        listData = new ArrayList<>();
        keys = new ArrayList<>();
        firebaseFirestore = FirebaseFirestore.getInstance();

    }

    private void loadData() {
        firebaseFirestore
                .collection("dasturkhan")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        listData = new ArrayList<>();
                        keys = new ArrayList<>();
                        QuerySnapshot queryDocumentSnapshots = task.getResult();
                        for (int i = 0; i < queryDocumentSnapshots.size(); i++) {
                            keys.add(queryDocumentSnapshots.getDocuments().get(i).getId());
                            listData.add(queryDocumentSnapshots.getDocuments().get(i).toObject(Data.class));
                        }
                        setDataToView(listData);
                    }
                });
    }

    public static MapFragment newInstance(ArrayList<MapModel> mapModels) {
        Bundle args = new Bundle();
        args.putParcelableArrayList("data", mapModels);
        MapFragment fragment = new MapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        markers = new ArrayList<>();
        mMap.setMyLocationEnabled(true);

        LatLng tashkent = new LatLng(41.27938829467854, 69.23891667276621);
        mMap.setOnMapClickListener(this);
        mMap.setOnMyLocationButtonClickListener(() -> {
            statusCheck();
            return true;
        });
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(tashkent, 10));
        loadData();
        mMap.setOnMarkerClickListener(marker -> {
            for (int i = 0; i < markers.size(); i++) {
                Log.d("MRX", String.valueOf(marker.getPosition() == markers.get(i).getPosition()));
                Log.d("MRX", String.valueOf(marker.getPosition()));
                if (marker.getPosition().latitude == markers.get(i).getPosition().latitude
                        && marker.getPosition().longitude == markers.get(i).getPosition().longitude) {
                    getActivity()
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.container2, UserBusyFragment.instance(keys.get(i), listData.get(i)))
                            .hide(getActivity().getSupportFragmentManager().findFragmentByTag("user_map"))
                            .addToBackStack("USER_BUSY")
                            .commit();
                }
            }
            return true;
        });
    }


    public void setDataToView(ArrayList<Data> data) {
        asyncTask = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                if (getActivity() != null && getUserVisibleHint() && !asyncTask.isCancelled()) {
                    paintMarker(data);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                cancel(true);
            }
        }.execute();
    }


    private void paintMarker(ArrayList<Data> mapModels) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.marker_icon, null, false);
        TextView textView = v.findViewById(R.id.name);
        ArrayList<BitmapDescriptor> icons = new ArrayList<>();
        for (int i = 0; i < mapModels.size(); i++) {
            textView.setText(mapModels.get(i).getModel().getName());
            icons.add(BitmapDescriptorFactory.fromBitmap(
                    createDrawableFromView(getContext(), v)
//                        getCroppedBitmap(Picasso.get()
//                        .load(R.drawable.location_pin)
//                        .resize(70, 70)
//                        .get())
            ));
        }
        Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
            for (int i = 0; i < listData.size(); i++) {
                BitmapDescriptor finalIcon = icons.get(i);
                Data d = mapModels.get(i);
                markers.add(mMap.addMarker(new MarkerOptions().position(new LatLng(d.getLocation().getLatitude(), d.getLocation().getLongitude())).icon(finalIcon)));
            }
        });
    }


    private Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    public Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

}
