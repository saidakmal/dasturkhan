package uz.N00003988.dasturkhan.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import uz.N00003988.dasturkhan.R;
import uz.N00003988.dasturkhan.settings.Settings;
import uz.N00003988.dasturkhan.ui.main.MainActivity;

public class AppMessagingService extends FirebaseMessagingService {
    private static final String TAG = "QWERTYUIOPP";
    private static final String NOTIFICATION_ID_EXTRA = "notificationId";
    private static final String IMAGE_URL_EXTRA = "imageUrl";
    private static final String ADMIN_CHANNEL_ID = "admin_channel";
    private NotificationManager notificationManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        super.onMessageReceived(remoteMessage);

        Intent notificationIntent = new Intent(this, MainActivity.class);
//        if (MainActivity.isAppRunning) {
//        } else {
//        }

        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        final PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0 /* Request code */, notificationIntent,
                PendingIntent.FLAG_ONE_SHOT);

        int notificationId = new Random().nextInt(60000);

//        Bitmap bitmap = getBitmapfromUrl(remoteMessage.getData().get("image-url"));
//        Intent likeIntent = new Intent(this, LikeService.class);
//        likeIntent.putExtra(NOTIFICATION_ID_EXTRA, notificationId);
//        likeIntent.putExtra(IMAGE_URL_EXTRA, remoteMessage.getData().get("image-url"));
//        PendingIntent likePendingIntent = PendingIntent.getService(this,
//                notificationId + 1, likeIntent, PendingIntent.FLAG_ONE_SHOT);


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels();
        }
        String message1 = "";
        String message2 = "";
        if (remoteMessage.getData().get("whoFor").equals("manager")) {
            message1 = "User name = " + remoteMessage.getData().get("name") + "\n" +
                    "User number = " + remoteMessage.getData().get("number") + "\n" +
                    "Date = " + remoteMessage.getData().get("date") + "\n" +
                    "Time = " + remoteMessage.getData().get("time") + "\n" +
                    "Table number = " + remoteMessage.getData().get("tableNumber") + "\n" +
                    "Name = " + remoteMessage.getData().get("nameItem") + "\n";
            message2 = "Manager";

        } else if (remoteMessage.getData().get("whoFor").equals("user")) {
            message1 = remoteMessage.getData().get("answer") + "\n";
            message2 = "User";
        }
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
//                        .setLargeIcon(bitmap)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Dasturkhan")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .setBigContentTitle("INFO")
                                .bigText(message1))
//                                .bigPicture(bitmap))/*Notification with Image*/
                        .setContentText(message2)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
//                        .addAction(R.drawable.ic_star_half_black_24dp,
//                                getString(R.string.notification_add_to_cart_button),likePendingIntent)
                        .setContentIntent(pendingIntent);

        notificationManager.notify(notificationId, notificationBuilder.build());
//        HashMap<String, String> hashMap = new HashMap<>();
//        hashMap.put("sfsf", "fsda");
//        FirebaseFirestore.getInstance().collection("notification").add(hashMap);

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
//            send2();
        }

    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        CharSequence adminChannelName = getString(R.string.notifications_admin_channel_name);
        String adminChannelDescription = getString(R.string.notifications_admin_channel_description);

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_HIGH);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }

    @Override
    public void onNewToken(String token) {
        Settings.getSettings().setToken(token);
        Log.d(TAG, "COMPLETE_SET_TOKEN: " + "onNewToken " + token);
    }


}
