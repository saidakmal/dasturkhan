package uz.N00003988.dasturkhan.retrofit.models;

import uz.N00003988.dasturkhan.ui.manager.notification.NotificationModel;

public class UserMessageData {
    private String topic;
    private UserMessageModel data;


    public UserMessageData() {
    }

    public UserMessageData(String topic, UserMessageModel data) {
        this.topic = topic;
        this.data = data;
    }

    public UserMessageData(String topic, String whoFor, String answer) {
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public UserMessageModel getData() {
        return data;
    }

    public void setData(UserMessageModel data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{\n" +
                " \"to\":\"/topics/" + topic + "\",\n" +
                " \"data\": {\n" +
                " \"whoFor\":\"" + data.getWhoFor() + "\",\n" +
                " \"answer\":\"" + data.getAnswer() + "\" \n" +
                " }\n" +
                "}";
    }
}
