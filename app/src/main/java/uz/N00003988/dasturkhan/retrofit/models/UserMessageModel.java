package uz.N00003988.dasturkhan.retrofit.models;

public class UserMessageModel {
    private String answer;
    private String whoFor;

    public UserMessageModel(String answer, String whoFor) {
        this.answer = answer;
        this.whoFor = whoFor;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getWhoFor() {
        return whoFor;
    }

    public void setWhoFor(String whoFor) {
        this.whoFor = whoFor;
    }
}
