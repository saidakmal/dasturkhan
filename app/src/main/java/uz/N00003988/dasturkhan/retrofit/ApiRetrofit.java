package uz.N00003988.dasturkhan.retrofit;


import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uz.N00003988.dasturkhan.settings.Settings;

public class ApiRetrofit {
    private static final String BASE_URL = "https://gcm-http.googleapis.com/gcm/";
        private static String apiKey = "AAAAMUNLeH8:APA91bGBEP9dSGtmvA2XY-TagbNp0b8jMF4h35XChDpRQcEXejE8AA9rJduqrBS99AXcoILCEtSRLERfyzWlHfdb2gFzZ-mgTHuPDlMgwF0bYlYdpDFnaZsOPvXHplZdynjvL1dtotc8";
    private static Retrofit retrofit;

    private static OkHttpClient getHttpClient() {
//        apiKey = Settings.getSettings().getToken();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request.Builder requestBuilder = original.newBuilder();
            requestBuilder.addHeader("Content-Type", "application/json");
            requestBuilder.addHeader("Authorization", "key=" + apiKey);
            Request request = requestBuilder.build();
            return chain.proceed(request);
        });
        return httpClient.build();
    }


    public static Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(getHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
