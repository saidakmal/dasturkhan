package uz.N00003988.dasturkhan.retrofit.models;

public class ResponseData {
    private long message_id;

    public ResponseData() {
    }

    public ResponseData(long message_id) {
        this.message_id = message_id;
    }

    public long getMessage_id() {
        return message_id;
    }

    public void setMessage_id(long message_id) {
        this.message_id = message_id;
    }
}
