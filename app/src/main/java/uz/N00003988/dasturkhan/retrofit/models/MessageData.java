package uz.N00003988.dasturkhan.retrofit.models;

import uz.N00003988.dasturkhan.ui.manager.notification.NotificationModel;

public class MessageData {
    private String topic;
    private NotificationModel data;

    public MessageData() {
    }

    public MessageData(String topic, NotificationModel data) {
        this.topic = topic;
        this.data = data;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }


    public NotificationModel getData() {
        return data;
    }

    public void setData(NotificationModel data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{\n" +
                " \"to\":\"/topics/" + topic + "\",\n" +
                " \"data\": {\n" +
                " \"whoFor\":\"" + data.getWhoFor() + "\",\n" +
                " \"userId\":\"" + data.getUserId() + "\",\n" +
                " \"name\":\"" + data.getName() + "\",\n" +
                " \"number\":\"" + data.getNumber() + "\",\n" +
                " \"tableNumber\":\"" + data.getTableNumber() + "\",\n" +
                " \"time\":\"" + data.getTime() + "\",\n" +
                " \"date\":\"" + data.getDate() + "\",\n" +
                " \"managerId\":\"" + data.getManagerId() + "\",\n" +
                " \"idItem\":\"" + data.getIdItem() + "\",\n" +
                " \"nameItem\":\"" + data.getNameItem() + "\",\n" +
                " \"managerAnswered\":\"" + data.getManagerAnswered() + "\",\n" +
                " \"accepted\":\"" + data.getAccepted() + "\" \n" +
                " }\n" +
                "}";
    }
}
